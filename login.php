﻿<?php require 'include/settings.php'; ?>

<?php
    session_start();
    if (isset($_SESSION['l_user']) && isset($_SESSION['l_hash']))
    {
        header("Location: index.php");
        die();
    }
    
    if (isset($_POST['email']) && isset($_POST['password']))
    {
        $email = $_POST['email'];
        $password = hash_hmac ( "sha512" , $_POST['password'], $g_salt);

        if ($mysqli->connect_errno) {
            printf("Connect failed: %s\n", $mysqli->connect_error);
            exit();
        }
        
        $email = $mysqli->real_escape_string($email);
        $password = $mysqli->real_escape_string($password);
        
        $result = $mysqli->query("SELECT * FROM s2gm.s2gm_users WHERE email=\"$email\" AND pass=\"$password\"");
        
        if ($row = $result->fetch_row())
        {
            $_SESSION['l_userid'] = $row[0];
            $_SESSION['l_user'] = $email;
            $_SESSION['l_hash'] = $password;
            $_SESSION['l_admin'] = $row[3];
            
            $result->close();
            $mysqli->close();
            header("Location: index.php");
            die();
        }
        else
        {
            $error = '<div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            The username and password entered do not match our records. Please try again.</div>';
        }
    } else {
        if (isset($_POST['e']))
        {
            $error = '<div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
            if ($_POST['e'] == 1)
            {
                echo 'Session Expired. Please login again.';
            }
            else
            {
                echo 'Invalid Session';
            }
            echo '</div>';
        }
    }
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <?php require 'modules/metadata.php'; ?>

    <?php require 'modules/css.php'; ?>
</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="login-panel panel panel-default" style="margin-top:5%;">
                    <div class="panel-heading">
                        <center><img src="media/s2gm.png" style="width:40%;" /></center>
                        <h1 class="panel-title" style="font-size:20px"><center><b>Selection and Scaling of Ground Motions</b><br><br>S<sup>2</sup>GM Version 1.0.0.</center></h1>
                    </div>
                    <div class="panel-body">
                        <?php if(isset($error)) echo $error; ?>
                        <div class="well" style="width:50%; float:left; padding-right:15px; text-align:justify;">
                            <p>
                                <h4>Welcome to S<sup>2</sup>GM Version 1.0.0.</h4><br>
                                S<sup>2</sup>GM is a web-based tool used to facilitate the selection, scaling, and downloading of ground motion time history records and data.
                                S<sup>2</sup>GM can be used to scale and select time history records from a continuously expanding database of records from different earthquake sources.
                                <br><br>The S<sup>2</sup>GM database contains an extensive set of records recorded in worldwide crustal, subcrustal (intraslab), and subduction (interface) regions. This is important in complex seismic settings, such as the Cascadia subduction zone, where the seismic hazard comes from multiple sources.

                            </p>
                        </div>
                        <div style="width:50%; float:right; padding-left:15px;">
                            <form role="form" method="post">
                                <fieldset>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Username" name="email" type="text" autofocus>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Password" name="password" type="password" value="">
                                    </div>
                                    <button type="submit" class="btn btn-lg btn-success btn-block">Login</button>
                                </fieldset>
                            </form>
                            <div class="well" style="position:absolute; float:right; bottom:7.5%; margin-bottom:3px; margin-right:4%; text-align:justify;">
                                The website and record database are continuously being updated and expanded. Comments on this tool are greatly appreciated.<br><br>
                                Please contact us at:<br><br><a href="mailto:admin@hpcperformancedesign.com">admin@hpcperformancedesign.com</a>
                            </div>
                        </div>
                    </div>
                </div>
                &copy; 2015 HPCPerformanceDesign. All rights reserved.
            </div>
        </div>
    </div>

    <?php require 'modules/js.php'; ?>

</body>

</html>