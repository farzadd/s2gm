<?php
    if ($_FILES['userdefspec']['type'] != "application/vnd.ms-excel")
    {
        header("Location: userdefinedspec.php?error=1&e=" . $_FILES['userdefspec']['type']);
        die();
    } else if ($_FILES['userdefspec']['size'] > 2000)
    {
        header("Location: userdefinedspec.php?error=2&e=" . $_FILES['userdefspec']['size']);
        die();
    }

    $row = 1;
    $aData[] = '';
    if (($handle = fopen($_FILES['userdefspec']['tmp_name'], "r")) !== FALSE) {
        while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
            $num = count($data);
            if ($num < 2) continue;
            
            $row++;
            if ($row < 5) continue;
            
            $aData .= $data;
        }
        fclose($handle);
    }
?>