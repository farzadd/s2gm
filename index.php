<?php require 'include/settings.php'; ?>
<?php require 'include/confirm_login.php'; ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <?php require 'modules/metadata.php'; ?>

    <?php require 'modules/css.php'; ?>
</head>

<body>

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0;">
            <?php require 'modules/header.php'; ?>
            <!-- /.navbar-header -->

            <?php require 'modules/toplinks.php'; ?>
            <!-- /.navbar-top-links -->

            <?php require 'modules/sidebar.php'; ?>
            <!-- /.navbar-static-side -->
        </nav>
        
        <?php require 'modules/footer.php'; ?>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Welcome to S<sup>2</sup>GM Selection and Scaling Database</h1>
                        <div class="well" style="text-align:justify;">
                            S<sup>2</sup>GM will scale and select ground motions based on an input target spectrum. Spectra can be user input or generated automatically for all localities in British Columbia using 2010 and 2015 hazard values.<br><br>
                            Once a target spectrum is specified, the user can select and download ground motions from three extensive databases of records. Records can be selected based on distance (epicentral or hypocentral), duration (5-95 significant duration), magnitude, site characteristics (30m shear wave velocity), or scaling factor range. Records may be unscaled or scaled to the target spectrum at a single period or over a weighted period range. Records are chosen to best match the spectral values of the target acceleration spectrum.<br><br>
                            The S<sup>2</sup>GM contains three databases of records recorded in worldwide crustal, subcrustal (intraslab), and subduction (interface) regimes. This allows appropriate records to be chosen for any location and any seismic setting. These databases are continuously expanding and being updated.
                        </div>
                        <div class="well" style="text-align:justify;">
                            <img src="media/cascadia.png" align="right" style="margin-left:1%;" />
                            Crustal Earthquakes occur from slips along faults in Earth’s crust, typically less than 35km deep. Crustal earthquakes are the most common worldwide and have been recorded during many strong shaking events. The S<sup>2</sup>GM database features over 3500 unique crustal records from over 100 major historic earthquakes.<br><br>
                            Subcrustal (Intraslab) Earthquakes occur deep within tectonic plates, typically 35km or deeper. Major subcrustal events have been recorded around the globe. The S<sup>2</sup>GM database features over 350 records from historic subcrustal earthquakes such as: Nisqually, Washington; (2001); Geiyo, Japan (2001); Geurrero, Mexico (1995); and Michoacan, Mexico (1997).<br><br>
                            Subduction (Interface) Earthquakes are major shaking events caused by slip between subducting tectonic plates. Subduction regimes have been responsible for the most intense earthquakes ever recorded. The S<sup>2</sup>GM database features over 1100 records from historic subduction earthquakes such as: Tohoku, Japan (2011); Hokkaido, Japan (2003); Michoacan, Mexico (1985); and El Maule, Chile (2010). 
                            All downloaded records are as-recorded (unscaled and unrotated).<br><br>
                            <div style="text-align:left;">Figure: <i>Cascadia earthquake sources from United States Geological Survey.</i></div><br>
                        </div>
                        Any problems encountered or comments should be directed to: <a href="mailto:admin@hpcperformancedesign.com">admin@hpcperformancedesign.com</a>
                        <br><br>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->

    <?php require 'modules/js.php'; ?>

</body>

</html>
