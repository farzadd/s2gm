<?php require 'include/settings.php'; ?>
<?php require 'include/confirm_login.php'; ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <?php require 'modules/metadata.php'; ?>

    <?php require 'modules/css.php'; ?>
</head>

<body>

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0;">
            <?php require 'modules/header.php'; ?>
            <!-- /.navbar-header -->

            <?php require 'modules/toplinks.php'; ?>
            <!-- /.navbar-top-links -->

            <?php require 'modules/sidebar.php'; ?>
            <!-- /.navbar-static-side -->
        </nav>

        <?php require 'modules/footer.php'; ?>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h2 class="page-header">Search Records to Match Target Spectrum</h2>
						<div class="form-group">
                            <script>
                                function loadSample()
                                {
                                    var r = confirm("Are you sure you want to replace your current values with sample values?");
                                    if (r == true)
                                    {
                                        var elems = document.body.getElementsByTagName("input");
                                        for(i = 0; i < elems.length; i++)
                                        {
                                            if (elems[i].getAttribute("placeholder") != "")
                                                elems[i].value = elems[i].getAttribute("placeholder");
                                        }
                                    }
                                }
                            </script>
                            <button type="submit" class="btn btn-success btn-lg" style="margin-left:9px;" onclick="loadSample();">Load Sample Input</button>
							<br>
                            <form method="post" action="searchprogress.php">
                                <table class="table">
                                    <tr>
                                        <td style="border-top: 0;">
                                            <div class="panel panel-default">
                                                <div class="panel-body">
                                                <table class="table" style="width:100%; float:left;">
                                                    <tr>
                                                        <td style="width:60%; border-top: 0;">
                                                            <label for="GMType">Select Ground Motion Type: </label>
                                                        </td>
                                                        <td style="width:40%; border-top: 0;">
                                                            <select name="GMType" class="form-control" style="width:180px">
                                                                <option value="1">Crustal</option>
                                                                <option value="2">Subcrustal</option>
                                                                <option value="3">Subduction</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                </table>
                                                </div>
                                            </div>
                                            <div class="panel panel-default">
                                                <div class="panel-body">
                                                    <table class="table" style="width:100%; float:left">
                                                        <tr>
                                                            <td style="width:60%; border-top: 0;">
                                                                <label for="Magnitude">Magnitude:<br>(Min,Max)</label>
                                                            </td>
                                                            <td style="width:40%; border-top: 0;">
                                                                <div class="form-group">
                                                                <input name="Magnitude" class="form-control" placeholder="5,8" style="width:180px">
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width:60%">
                                                                <label for="Epidistance">Epicentral Distance (km):<br>(Min,Max)</label>
                                                            </td>
                                                            <td style="width:40%">
                                                                <div class="form-group">
                                                                <input name="Epidistance" class="form-control" placeholder="0,300" style="width:180px">
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width:60%">
                                                                <label for="Hypdistance">Hypocentral Distance (km):<br>(Min,Max)</label>
                                                            </td>
                                                            <td style="width:40%">
                                                                <div class="form-group">
                                                                <input name="Hypdistance" class="form-control" placeholder="0,300" style="width:180px">
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width:60%">
                                                                <label for="Soiltype">Vs30 (m/s):<br>(Min,Max)</label>
                                                            </td>
                                                            <td style="width:40%">
                                                                <div class="form-group">
                                                                <input name="Soiltype" class="form-control" placeholder="450,760" style="width:180px">
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width:60%">
                                                                <label for="D595">D5-95 (sec):<br>(Min,Max)</label>
                                                            </td>
                                                            <td style="width:40%">
                                                                <div class="form-group">
                                                                <input name="D595" class="form-control" placeholder="0,200" style="width:180px">
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <td style="width:60%">
                                                            <label for="Pulse">Pulse: </label>
                                                        </td>
                                                        <td style="width:40%">
                                                            <select name="Pulse" class="form-control" style="width:180px">
                                                                <option value="1">Any Record</option>
                                                            </select>
                                                        </td>
                                                    </table>
                                                </div>
                                            </div>
                                        </td>
                                        <td style="width:50%; border-top: 0;">
                                            <div class="panel panel-default">
                                                <div class="panel-body">
                                                    <table class="table" style="width:100%; float:left;">
                                                        <tr>
                                                            <td style="width:60%; border-top: 0;">
                                                                <label for="Direction">Spectral Ordinate: </label>
                                                            </td>
                                                            <td style="width:40%; border-top: 0;">
                                                                <select name="Direction" class="form-control" style="width:180px">
                                                                    <option value="1">H1</option>
                                                                    <option value="2">H2</option>
                                                                    <option value="3">V</option>
                                                                    <option value="4">SRSS</option>
                                                                    <option value="5">Geomean</option>
                                                                    <option value="6">RotD100</option>
                                                                    <option value="7">RotD50</option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width:60%">
                                                                <label for="NoGM">Max No. Records:<br>(<=10)</label>
                                                            </td>
                                                            <td style="width:40%">
                                                                <div class="form-group">
                                                                <input name="NoGM" class="form-control" placeholder="10" style="width:180px">
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width:60%">
                                                                <label for="ScalingMeth">Select Scaling Method:</label>
                                                            </td>
                                                            <td style="width:40%">
                                                                <script>
                                                                    function scalingChanged()
                                                                    {
                                                                        var value = document.getElementById("ScalingMeth").value;
                                                                        
                                                                        if (value == "1")
                                                                        {
                                                                            document.getElementById("ScalePeriod").disabled = true;
                                                                            document.getElementById("ScaleFact").disabled = true;
                                                                        } else if (value == "2")
                                                                        {
                                                                            document.getElementById("ScalePeriod").disabled = true;
                                                                            document.getElementById("ScaleFact").disabled = false;
                                                                        } else if (value == "3")
                                                                        {
                                                                            document.getElementById("ScalePeriod").disabled = false;
                                                                            document.getElementById("ScaleFact").disabled = false;
                                                                        }
                                                                    }
                                                                    
                                                                    window.onload = function () {
                                                                        scalingChanged();
                                                                    }
                                                                </script>
                                                                <select name="ScalingMeth" id="ScalingMeth" class="form-control" style="width:180px" onchange="scalingChanged();">
                                                                    <option value="1">No Scaling</option>
                                                                    <option value="2">Minimize MSE</option>
                                                                    <option value="3">Single Period</option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width:60%"> 
                                                                <label for="ScalePeriod">Scaling Period (sec):<br>(Ti)</label>
                                                            </td>
                                                            <td style="width:40%">
                                                                <div class="form-group">
                                                                <input name="ScalePeriod" id="ScalePeriod" class="form-control" placeholder="1" style="width:180px" disabled>
                                                                </div>	
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width:60%"> 
                                                                <label for="ScaleFact">Scale Factor:<br>(Min,Max)</label>
                                                            </td>
                                                            <td style="width:40%">
                                                                <div class="form-group">
                                                                <input name="ScaleFact" id="ScaleFact" class="form-control" placeholder="0.5,2.5" style="width:180px" disabled>
                                                                </div>	
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width:60%"> 
                                                                <label for="PeriodPoint">Period Points:<br>(T1,T2,...,Tn)</label>
                                                            </td>
                                                            <td style="width:40%">
                                                                <div class="form-group">
                                                                <input name="PeriodPoint" class="form-control" placeholder="0.5,1.5" style="width:180px">
                                                                </div>	
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width:60%"> 
                                                                <label for="Weights">Weights:<br>(W1,W2,...,Wn)</label>
                                                            </td>
                                                            <td style="width:40%">
                                                                <div class="form-group">
                                                                <input name="Weights" class="form-control" placeholder="1.0,1.0" style="width:180px">
                                                                </div>	
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width:60%"> 
                                                                <label for="PerDis">Period Discretization:<br>(e.g. 0.1)</label>
                                                            </td>
                                                            <td style="width:40%">
                                                                <div class="form-group">
                                                                <input name="PerDis" class="form-control" placeholder="0.1" style="width:180px">
                                                                </div>	
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <center><button type="submit" class="btn btn-primary btn-lg" style="width:20%;">Submit</button></center>
                            </form>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->

    <?php require 'modules/js.php'; ?>

</body>

</html>
