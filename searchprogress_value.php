<?php
    require 'include/settings.php';

	$mysqli = new mysqli($g_db_host, $g_db_user, $g_db_pass, $g_db_name, $g_db_port);

    if ($mysqli->connect_errno) {
        printf("Connect failed: %s\n", $mysqli->connect_error);
        exit();
    }
    
    $search_id = (isset($_GET['id']) ? $_GET['id'] : 0);
    
    if ($search_id == 0)
        exit("Failed! Search not specified!");
    
    session_start();
    $result = $mysqli->query("SELECT * FROM s2gm.s2gm_searchs WHERE id=$search_id AND user_id=". $_SESSION['l_userid']. ";");
    
    if ($row = $result->fetch_row())
    {
        if (empty($row[3]))
            echo $row[2];
        else
            exit("Failed! " . $row[3]);
    }
    else
        exit("Failed! Search not found!");
?>