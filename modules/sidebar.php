<div class="navbar-default sidebar" role="navigation" >
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
            <li>
                <a href="targetspectrum.php"><i class="fa fa-bar-chart-o fa-fw"></i> Target Response Spectrum</a>
            </li>
            <ul class="nav nav-second-level">
                <li>
                    <a href="bcdatabase.php"><i class="fa  fa-database fa-fw"></i> British Columbia Database</a>
                </li>
                <li>
                    <a href="userdefinedspec.php"><i class="fa fa-user fa-fw"></i> User Defined</a>
                </li>
                <li>
                    <a href="gmpe.php"><i class="fa fa-sitemap fa-fw"></i> GM Prediction Equationss</a>
                </li>
            </ul>
            <li></li>
            <?php if (isset($_COOKIE['user_data']) || isset($user_data)): ?>
            <li>
                <a href="searchrecords.php"><i class="fa fa-search fa-fw"></i> Search Records</a>
            </li>
            <?php endif; ?>
        </ul>
        
    </div>
    <!-- /.sidebar-collapse -->
</div>