<ul class="nav navbar-top-links navbar-right">
    <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
            <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
        </a>
        <ul class="dropdown-menu dropdown-user">
            <?php if (isset($_SESSION['l_user'])): ?>
            <li><a href="profile.php"><i class="fa fa-user fa-fw"></i> <?php echo $_SESSION['l_user']; ?></a></li>
            <li class="divider"></li>
            <?php endif; ?>
            <li><a href="logout.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a></li>
        </ul>
        <!-- /.dropdown-user -->
    </li>
    <!-- /.dropdown -->
</ul>