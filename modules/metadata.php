<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="HPCPerformanceDesign">

<link rel="apple-touch-icon" sizes="57x57" href="media/apple-touch-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="media/apple-touch-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="media/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="media/apple-touch-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="media/apple-touch-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="media/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="media/apple-touch-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="media/apple-touch-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="media/apple-touch-icon-180x180.png">
<link rel="icon" type="image/png" href="media/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="media/android-chrome-192x192.png" sizes="192x192">
<link rel="icon" type="image/png" href="media/favicon-96x96.png" sizes="96x96">
<link rel="icon" type="image/png" href="media/favicon-16x16.png" sizes="16x16">
<link rel="manifest" href="media/manifest.json">

<link rel="icon" type="image/png" href="media/favicon-96x96.png">
<link rel="shortcut icon" href="media/favicon.ico">

<meta name="msapplication-TileColor" content="#ffc40d">
<meta name="msapplication-TileImage" content="media/mstile-144x144.png">
<meta name="msapplication-config" content="media/browserconfig.xml">
<meta name="theme-color" content="#ffffff">

<title>Selection and Scaling of Ground Motions</title>