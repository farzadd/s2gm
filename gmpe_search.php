<?php require 'include/settings.php'; ?>
<?php require 'include/confirm_login.php'; ?>

<?php
    if (!isset($_POST['gmpes']))
    {
        header("Location: gmpe.php");
        exit();
    }
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <?php require 'modules/metadata.php'; ?>

    <?php require 'modules/css.php'; ?>
</head>

<body>

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0;">
            <?php require 'modules/header.php'; ?>
            <!-- /.navbar-header -->

            <?php require 'modules/toplinks.php'; ?>
            <!-- /.navbar-top-links -->

            <?php require 'modules/sidebar.php'; ?>
            <!-- /.navbar-static-side -->
        </nav>
        
        <?php require 'modules/footer.php'; ?>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Searching Database...</h1>
                        <?php
                            if ($mysqli->connect_errno) {
                                printf("Connect failed: %s\n", $mysqli->connect_error);
                                exit();
                            }

                            $damp = mysqli_real_escape_string($mysqli, getPost('damp'));
                            $region = mysqli_real_escape_string($mysqli, getPost('region'));
                            $fault_type = mysqli_real_escape_string($mysqli, getPost('faultType'));
                            $magnitude = mysqli_real_escape_string($mysqli, getPost('magnitude'));
                            $rrup = mysqli_real_escape_string($mysqli, getPost('rrup'));
                            $rx = mysqli_real_escape_string($mysqli, getPost('rx'));
                            $ry0 = mysqli_real_escape_string($mysqli, getPost('ry0'));
                            $rjb = mysqli_real_escape_string($mysqli, getPost('rjb'));
                            $rcd = mysqli_real_escape_string($mysqli, getPost('rcd'));
                            $df = mysqli_real_escape_string($mysqli, getPost('df'));
                            $ztor = mysqli_real_escape_string($mysqli, getPost('ztor'));
                            $zbot = mysqli_real_escape_string($mysqli, getPost('zbot'));
                            $width = mysqli_real_escape_string($mysqli, getPost('width'));
                            $delta = mysqli_real_escape_string($mysqli, getPost('delta'));
                            $lambda = mysqli_real_escape_string($mysqli, getPost('lambda'));
                            $vs30 = mysqli_real_escape_string($mysqli, getPost('vs30'));
                            $vs30type = mysqli_real_escape_string($mysqli, getPost('vs30type'));
                            $z10 = mysqli_real_escape_string($mysqli, getPost('z10'));
                            $z25 = mysqli_real_escape_string($mysqli, getPost('z25'));
                            $zhyp = mysqli_real_escape_string($mysqli, getPost('zhyp'));
                            $hw = mysqli_real_escape_string($mysqli, getPost('hw'));
                            $szt = mysqli_real_escape_string($mysqli, getPost('szt'));
                            $faba = mysqli_real_escape_string($mysqli, getPost('faba'));
                            $jscfcorr = mysqli_real_escape_string($mysqli, getPost('jscfcorr'));
                            $ba08corr = mysqli_real_escape_string($mysqli, getPost('ba08corr'));
                            $zi = mysqli_real_escape_string($mysqli, getPost('zi'));
                            $zr = mysqli_real_escape_string($mysqli, getPost('zr'));
                            $arb = mysqli_real_escape_string($mysqli, getPost('arb'));
                            $fas = mysqli_real_escape_string($mysqli, getPost('fas'));

                            $query = "INSERT INTO `s2gm`.`s2gm_gmpe_search`
                                    (`gmpe_ids`,
                                    `gmpe_weights`,
                                    `damp`,
                                    `region`,
                                    `fault_type`,
                                    `magnitude`,
                                    `rrup`,
                                    `rx`,
                                    `ry0`,
                                    `rjb`,
                                    `rcd`,
                                    `df`,
                                    `ztor`,
                                    `zbot`,
                                    `width`,
                                    `delta`,
                                    `lambda`,
                                    `vs30`,
                                    `vs30type`,
                                    `z10`,
                                    `z25`,
                                    `zhyp`,
                                    `hw`,
                                    `szt`,
                                    `faba`,
                                    `jscfcorr`,
                                    `ba08corr`,
                                    `zi`,
                                    `zr`,
                                    `arb`,
                                    `fas`)
                                    VALUES
                                    ('4',
                                    '1',
                                    $damp,
                                    $region,
                                    $fault_type,
                                    $magnitude,
                                    $rrup,
                                    $rx,
                                    $ry0,
                                    $rjb,
                                    $rcd,
                                    $df,
                                    $ztor,
                                    $zbot,
                                    $width,
                                    $delta,
                                    $lambda,
                                    $vs30,
                                    $vs30type,
                                    $z10,
                                    $z25,
                                    $zhyp,
                                    $hw,
                                    $szt,
                                    $faba,
                                    $jscfcorr,
                                    $ba08corr,
                                    $zi,
                                    $zr,
                                    $arb,
                                    $fas);";

                            if (!$result = $mysqli->query($query))
                                echo "Search Failed, error: ", $mysqli->error;

                            $search_id = mysqli_insert_id($mysqli);
                            
                            exec("C:\test.txt");
                        ?>
                        <?php
                            function getPost($name)
                            {
                                if (isset($_POST[$name]))
                                {
                                    if ($_POST[$name] != "")
                                        return $_POST[$name];
                                    else
                                        return 0;
                                }
                                else
                                    return 0;
                            }
                        ?>
                        <script type="text/javascript" src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
                        <script>
                            $(document).ready(
                                    function() {
                                        setInterval(function() {
                                            var value = $.get('gmpe_search_progress.php', { id: "<?php echo $search_id; ?>"} )
                                              .done(function( data ) {
                                                if (isNaN(data))
                                                {
                                                    $('#progBar').text(data);
                                                    $('#progBar').css('width', '100%').attr('aria-valuenow', 100);
                                                } else
                                                {
                                                    if (data == "100")
                                                    {
                                                        window.location.replace("gmpe_search_complete.php?search_id=<?php echo $search_id; ?>");
                                                    }
                                                    else if (data == "0")
                                                    {
                                                        $('#progBar').text("Initiating search. Please wait...");
                                                        $('#progBar').css('width', '100%').attr('aria-valuenow', 100);
                                                    }
                                                    else
                                                    {
                                                        $('#progBar').text(data+'%');
                                                        $('#progBar').css('width', data+'%').attr('aria-valuenow', data);
                                                    }
                                                }
                                              });;
                                        }, 500);
                                    });
                        </script>
                        <div class="progress">
                          <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 100%;" id="progBar">
                            Initiating search. Please wait...
                          </div>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->

    <?php require 'modules/js.php'; ?>

</body>

</html>
