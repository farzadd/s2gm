<?php require 'include/settings.php'; ?>
<?php require 'include/confirm_login.php'; ?>

<!DOCTYPE html>

<?php
    if (isset($_POST['t']))
    {
        if ($_FILES['userdefspec']['type'] != "application/vnd.ms-excel")
        {
            header("Location: userdefinedspec.php?error=1&e=" . $_FILES['userdefspec']['type']);
            die();
        } else if ($_FILES['userdefspec']['size'] > 5000)
        {
            header("Location: userdefinedspec.php?error=2&e=" . $_FILES['userdefspec']['size']);
            die();
        }

        $row = 1;
        $aData = array();
        if (($handle = fopen($_FILES['userdefspec']['tmp_name'], "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $num = count($data);
                if ($num < 2) continue;
                
                $row++;
                if ($row < 5) continue;
                
                array_push($aData, $data);
            }
            fclose($handle);
        }
    }
?>

<html lang="en">

<head>
    <?php require 'modules/metadata.php'; ?>

    <script src="/js/ajaxupload.js" type="text/javascript"></script>
    
    <?php require 'modules/css.php'; ?>
</head>

<body>

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0;">
            <?php require 'modules/header.php'; ?>
            <!-- /.navbar-header -->

            <?php require 'modules/toplinks.php'; ?>
            <!-- /.navbar-top-links -->

            <?php require 'modules/sidebar.php'; ?>
            <!-- /.navbar-static-side -->
        </nav>
        
        <?php require 'modules/footer.php'; ?>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h2 class="page-header">Target Response Spectrum: User Defined</h2>
						<div class="form-group">
                            <br>
                            <br>
                            <?php
                                if(isset($_POST['error']))
                                {
                                    if ($_POST['error'] == 1)
                                    {
                                        echo '<div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                The file type is not a valid. Only accepted values are comma-separated value files. The file uploaded was: ' . (isset($_POST['e']) ? $_POST['e'] : "Unknown") . '.</div>';
                                    }
                                    else if ($_POST['error'] == 2)
                                    {
                                        echo '<div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                The uploaded file\'s size was too large (5KB) . Uploaded file size: ' . (isset($_POST['e']) ? $_POST['e'] : "Unknown") . ' bytes.</div>';
                                    }
                                }
                            ?>
                            <form method="post" enctype="multipart/form-data" action="userdefinedspec.php" name="upload_csv" id="upload_csv">
                                <input type="hidden" name="t" value="ufs" />
                                <table class="table" style="width:50%; float:left;">
                                    <tr>
                                        <td style="border-top: 0;">As shown in the sample file, start spectra data at row 4 of input file. <br> Spectra data consists of rows of T,pSa comma-separated values. </td>
                                        <td style="border-top: 0;">
                                            <a type="button" class="btn btn-info" href="csv/SpectraSample.csv">Download Sample File (.csv)</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:60%">
                                            <label for="userdefspec">Select Spectrum File: </label>
                                        </td>
                                        <td style="width:40%">
                                            <div class="form-group">
                                                <input name="userdefspec" id="userdefspec" type="file" accept=".csv" onchange="document.getElementById('upload_csv').submit();">
                                            </div>	
                                        </td>
                                    </tr>
                                </table>
                            </form>
                            <?php if (isset($_POST['t'])): ?>
                                <form method="post" action="targetspectrum.php">
                                    <div class="table-responsive" style="padding-left: 50px; width:50%; float:right;">
                                        <table class="table table-striped table-bordered table-hover">
                                            <thead>
                                                <th>Period (sec)</th>
                                                <th>Spectral Acceleration (g)</th>
                                            </thead>
                                            <?php
                                                $aData = array_reverse($aData);
                                                $tempCopy = $aData;
                                                while ($data = array_pop($tempCopy))
                                                    echo "<tr><td>$data[0]</td><td>$data[1]</td></tr>";
                                            ?>
                                        </table>
                                        <input type="hidden" name="request_type" value="user_defined" />
                                        <input type="hidden" name="user_data" value='<?php echo json_encode($aData); ?>' />
                                        <br><br>
                                    </div>
                                    <button type="submit" id="confirm" name="confirm" class="btn btn-primary btn-lg" style="width:13%; float:right; margin-right:6.5%;">Submit</button>
                                </form>
                            <?php endif; ?>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->

    <?php require 'modules/js.php'; ?>

</body>

</html>
