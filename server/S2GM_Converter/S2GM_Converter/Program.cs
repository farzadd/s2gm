﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;
using MySql.Data.MySqlClient;
using System.Text.RegularExpressions;

namespace S2GM_Converter
{
    class Program
    {
        private static MySqlConnection connection;
        private static MySqlCommand createSpectra;

        static void Main(string[] args)
        {
            // Connect to the database
            string connectionString = "SERVER=" + "daei.ca" + ";" + "DATABASE=" +
                    "s2gm" + ";" + "UID=" + "s2gm" + ";" + "PASSWORD=\"" + "armin128" + "\";";

            connection = new MySqlConnection(connectionString);
            connection.Open();

            // Create the queries
            string query =
                    "INSERT INTO `s2gm`.`s2gm_gmaccel` (`name`, `fullname`, `unit`, `timestep`, `time_interval`, `PA`) VALUES (?name, ?fullname, ?unit, ?timestep, ?time_interval, ?PA);";
            createSpectra = new MySqlCommand(query, connection);

            // Read all the files
            string curDir = Directory.GetCurrentDirectory();

            foreach (string file in Directory.GetFiles(curDir))
            {
                // Convert .dat files
                if (file.EndsWith(".dat"))
                    convertFile(file);
            }

            // Close the connection
            connection.Close();

            Console.WriteLine("All files have been imported");
            Console.ReadLine();
        }

        public static void convertFile(string file)
        {
            string fileContent = File.ReadAllText(file);

            string[] fileLines = fileContent.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.RemoveEmptyEntries);

            if (fileLines.Length > 5)
            {
                string name = Path.GetFileName(file);
                string fullName = fileLines[0].Trim();
                string unit = fileLines[1].Replace("unit ", "");
                string timeSteps = fileLines[2].Replace("timesteps ", "");
                string timeInterval = fileLines[3].Replace("time interval ", "");
                string PA = fileLines[4];
                Regex rgx = new Regex("[^0-9,.-]");
                PA = rgx.Replace(PA, "");

                createSpectra.Parameters.Clear();
                createSpectra.Parameters.AddWithValue("?name", name);
                createSpectra.Parameters.AddWithValue("?fullname", fullName);
                createSpectra.Parameters.AddWithValue("?unit", unit);
                createSpectra.Parameters.AddWithValue("?timestep", timeSteps);
                createSpectra.Parameters.AddWithValue("?time_interval", timeInterval);
                createSpectra.Parameters.AddWithValue("?PA", PA);

                createSpectra.Prepare();
                createSpectra.ExecuteNonQuery();

                long id = createSpectra.LastInsertedId;
                Console.WriteLine("Importing: [" + id + "]: " + name + ";" + fullName + ";" + unit + ";" + timeSteps + ";" + timeInterval + ";" + PA);

                string query = "INSERT INTO `s2gm`.`s2gm_gmaccel_values` (`gmaccel_id`, `value`) VALUES ";

                string values = "";
                for (int i = 5; i < fileLines.Length; i ++)
                {
                    values += "(" + id + "," + fileLines[i] + "),";
                }
                values = values.Remove(values.Length - 1, 1);

                MySqlCommand createSpectraValue = new MySqlCommand(query + values, connection);
                createSpectraValue.Prepare();
                createSpectraValue.ExecuteNonQuery();
            }
        }
    }
}
