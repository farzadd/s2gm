﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Net.Sockets;
using System.Net;
using System.Diagnostics;

namespace S2GM_Server
{
    class Program
    {
        public static Thread listenThread;
        public static TcpListener listenServer;
        public static Int32 port;

        static void Main(string[] args)
        {
            Console.Write("Enter port number to listen to: ");
            port = Int32.Parse(Console.ReadLine());

            listenThread = new Thread(new ThreadStart(startListening));
            listenThread.Start();
        }

        public static void startListening()
        {
            listenServer = new TcpListener(IPAddress.Any, port);

            try
            {
                listenServer.Start();

                // Buffer for reading data
                byte[] bytes = new byte[1024];
                String data = null;

                Console.WriteLine("Listen Server is now active!");
                // Enter the listening loop. 
                while (true)
                {
                    Console.WriteLine("Waiting for request...");
                    TcpClient client = null;
                    try
                    {
                        client = listenServer.AcceptTcpClient();
                    }
                    catch (SocketException se)
                    {
                        if (!se.Message.Contains("WSACancel"))
                            Console.WriteLine(se.Message);
                        return;
                    }
                    Console.WriteLine("Connection Established!");

                    int i;
                    data = null;
                    NetworkStream stream = client.GetStream();

                    // Loop to receive all the data sent by the client. 
                    while ((i = stream.Read(bytes, 0, bytes.Length)) != 0)
                    {
                        data = System.Text.Encoding.ASCII.GetString(bytes, 0, i);
                        Console.WriteLine("Received request: " + data);

                        string response = handleCommand(data);

                        // Send back a response.
                        byte[] msg = System.Text.Encoding.ASCII.GetBytes(response);
                        stream.Write(msg, 0, msg.Length);

                        // Close the connection - shit code, fix later
                        client.Close();
                        break;
                    }
                }
            }
            catch (ThreadAbortException)
            {
                listenServer.Stop();
                Thread.ResetAbort();
            }
        }

        public static string handleCommand(string data)
        {
            Process.Start("S2GM.exe", data);
            return "success";
        }
    }
}
