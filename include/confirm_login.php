<?php
    session_start();
    if (!isset($_SESSION['l_user']) || !isset($_SESSION['l_hash']))
    {
        session_destroy();
        header("Location: login.php?e=1");
        die();
    }
    
    $email = $_SESSION['l_user'];
    $hash = $_SESSION['l_hash'];
    
    $mysqli = new mysqli($g_db_host, $g_db_user, $g_db_pass, $g_db_name, $g_db_port);

    if ($mysqli->connect_errno) {
        printf("Connect failed: %s\n", $mysqli->connect_error);
        exit();
    }
    
    $result = $mysqli->query("SELECT * FROM s2gm.s2gm_users WHERE email=\"$email\" AND pass=\"$hash\"");
    
    if (!$result->fetch_row())
    {
        session_destroy();
        header("Location: login.php?e=2");
    }
?>