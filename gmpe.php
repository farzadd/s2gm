<?php require 'include/settings.php'; ?>
<?php require 'include/confirm_login.php'; ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <?php require 'modules/metadata.php'; ?>

    <?php require 'modules/css.php'; ?>
</head>

<body>

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0;">
            <?php require 'modules/header.php'; ?>
            <!-- /.navbar-header -->

            <?php require 'modules/toplinks.php'; ?>
            <!-- /.navbar-top-links -->

            <?php require 'modules/sidebar.php'; ?>
            <!-- /.navbar-static-side -->
        </nav>
        
        <?php require 'modules/js.php'; ?>
        
        <?php require 'modules/footer.php'; ?>

        <!-- Javascript -->
        <script>
            var params = [
                'damp', 'region', 'faultType', 'magnitude', 'rrup', 'rx', 'ry0', 'rjb', 'rcd', 'df', 'ztor', 'zbot', 'width', 'delta', 'lambda', 'vs30', 'vs30type', 'z10', 'z25', 'zhyp', 'hw', 'szt', 'faba', 'jscfcorr', 'ba08corr', 'zi', 'zr', 'arb', 'fas'
            ]
        
            var paramMatrix = {
                '0': {
                    'damp' : '1', 'region' : '1', 'faultType' : '0', 'magnitude' : '1', 'rrup' : '1', 'rx' : '1', 'ry0' : '1', 'rjb' : '1', 'rcd' : '0', 'df' : '0', 'ztor' : '1', 'zbot' : '0', 'width' : '1', 'delta' : '1', 'lambda' : '1', 'vs30' : '1', 'vs30type' : '1', 'z10' : '1', 'z25' : '0', 'zhyp' : '0', 'hw' : '1', 'szt' : '0', 'faba' : '0', 'jscfcorr' : '0', 'ba08corr' : '0', 'zi' : '0', 'zr' : '0', 'arb' : '0', 'fas' : '1'
                },
                '1': {
                    'damp' : '1', 'region' : '1', 'faultType' : '1', 'magnitude' : '1', 'rrup' : '0', 'rx' : '0', 'ry0' : '0', 'rjb' : '1', 'rcd' : '0', 'df' : '0', 'ztor' : '0', 'zbot' : '0', 'width' : '0', 'delta' : '0', 'lambda' : '0', 'vs30' : '1', 'vs30type' : '1', 'z10' : '1', 'z25' : '0', 'zhyp' : '0', 'hw' : '0', 'szt' : '0', 'faba' : '0', 'jscfcorr' : '0', 'ba08corr' : '0', 'zi' : '0', 'zr' : '0', 'arb' : '0', 'fas' : '0'
                },
                '2': {
                    'damp' : '1', 'region' : '1', 'faultType' : '0', 'magnitude' : '1', 'rrup' : '1', 'rx' : '1', 'ry0' : '0', 'rjb' : '1', 'rcd' : '0', 'df' : '0', 'ztor' : '1', 'zbot' : '1', 'width' : '1', 'delta' : '1', 'lambda' : '1', 'vs30' : '1', 'vs30type' : '0', 'z10' : '0', 'z25' : '1', 'zhyp' : '1', 'hw' : '1', 'szt' : '0', 'faba' : '0', 'jscfcorr' : '0', 'ba08corr' : '0', 'zi' : '0', 'zr' : '0', 'arb' : '0', 'fas' : '0'
                },
                '3': {
                    'damp' : '1', 'region' : '1', 'faultType' : '0', 'magnitude' : '1', 'rrup' : '1', 'rx' : '1', 'ry0' : '0', 'rjb' : '1', 'rcd' : '0', 'df' : '0', 'ztor' : '1', 'zbot' : '0', 'width' : '0', 'delta' : '1', 'lambda' : '1', 'vs30' : '1', 'vs30type' : '1', 'z10' : '1', 'z25' : '0', 'zhyp' : '0', 'hw' : '1', 'szt' : '0', 'faba' : '0', 'jscfcorr' : '0', 'ba08corr' : '0', 'zi' : '0', 'zr' : '0', 'arb' : '0', 'fas' : '0'
                },
                '4': {
                    'damp' : '1', 'region' : '0', 'faultType' : '1', 'magnitude' : '1', 'rrup' : '1', 'rx' : '0', 'ry0' : '0', 'rjb' : '0', 'rcd' : '0', 'df' : '0', 'ztor' : '0', 'zbot' : '0', 'width' : '0', 'delta' : '0', 'lambda' : '0', 'vs30' : '1', 'vs30type' : '0', 'z10' : '0', 'z25' : '0', 'zhyp' : '0', 'hw' : '0', 'szt' : '0', 'faba' : '0', 'jscfcorr' : '0', 'ba08corr' : '0', 'zi' : '0', 'zr' : '0', 'arb' : '0', 'fas' : '0'
                },
                '5': {
                    'damp' : '1', 'region' : '0', 'faultType' : '1', 'magnitude' : '1', 'rrup' : '1', 'rx' : '1', 'ry0' : '0', 'rjb' : '0', 'rcd' : '0', 'df' : '0', 'ztor' : '1', 'zbot' : '0', 'width' : '1', 'delta' : '1', 'lambda' : '1', 'vs30' : '1', 'vs30type' : '1', 'z10' : '1', 'z25' : '0', 'zhyp' : '0', 'hw' : '1', 'szt' : '0', 'faba' : '0', 'jscfcorr' : '0', 'ba08corr' : '0', 'zi' : '0', 'zr' : '1', 'arb' : '0', 'fas' : '1'
                },
                '6': {
                    'damp' : '1', 'region' : '0', 'faultType' : '1', 'magnitude' : '1', 'rrup' : '0', 'rx' : '0', 'ry0' : '0', 'rjb' : '1', 'rcd' : '0', 'df' : '0', 'ztor' : '0', 'zbot' : '0', 'width' : '0', 'delta' : '0', 'lambda' : '0', 'vs30' : '1', 'vs30type' : '0', 'z10' : '0', 'z25' : '0', 'zhyp' : '0', 'hw' : '0', 'szt' : '0', 'faba' : '0', 'jscfcorr' : '0', 'ba08corr' : '0', 'zi' : '0', 'zr' : '0', 'arb' : '0', 'fas' : '0'
                },
                '7': {
                    'damp' : '1', 'region' : '0', 'faultType' : '0', 'magnitude' : '1', 'rrup' : '1', 'rx' : '0', 'ry0' : '0', 'rjb' : '1', 'rcd' : '0', 'df' : '0', 'ztor' : '1', 'zbot' : '0', 'width' : '0', 'delta' : '1', 'lambda' : '1', 'vs30' : '1', 'vs30type' : '0', 'z10' : '0', 'z25' : '1', 'zhyp' : '0', 'hw' : '1', 'szt' : '0', 'faba' : '0', 'jscfcorr' : '0', 'ba08corr' : '0', 'zi' : '0', 'zr' : '0', 'arb' : '1', 'fas' : '0'
                },
                '8': {
                    'damp' : '1', 'region' : '0', 'faultType' : '0', 'magnitude' : '1', 'rrup' : '1', 'rx' : '1', 'ry0' : '0', 'rjb' : '1', 'rcd' : '0', 'df' : '0', 'ztor' : '1', 'zbot' : '0', 'width' : '0', 'delta' : '1', 'lambda' : '1', 'vs30' : '1', 'vs30type' : '1', 'z10' : '1', 'z25' : '0', 'zhyp' : '0', 'hw' : '0', 'szt' : '0', 'faba' : '0', 'jscfcorr' : '0', 'ba08corr' : '0', 'zi' : '0', 'zr' : '0', 'arb' : '0', 'fas' : '1'
                },
                '9': {
                    'damp' : '1', 'region' : '0', 'faultType' : '1', 'magnitude' : '1', 'rrup' : '1', 'rx' : '0', 'ry0' : '0', 'rjb' : '0', 'rcd' : '0', 'df' : '0', 'ztor' : '0', 'zbot' : '0', 'width' : '0', 'delta' : '0', 'lambda' : '0', 'vs30' : '1', 'vs30type' : '0', 'z10' : '0', 'z25' : '0', 'zhyp' : '0', 'hw' : '0', 'szt' : '0', 'faba' : '0', 'jscfcorr' : '0', 'ba08corr' : '0', 'zi' : '0', 'zr' : '0', 'arb' : '0', 'fas' : '0'
                },
                '10': {
                    'damp' : '1', 'region' : '0', 'faultType' : '0', 'magnitude' : '1', 'rrup' : '0', 'rx' : '0', 'ry0' : '0', 'rjb' : '0', 'rcd' : '0', 'df' : '1', 'ztor' : '0', 'zbot' : '0', 'width' : '0', 'delta' : '0', 'lambda' : '0', 'vs30' : '1', 'vs30type' : '0', 'z10' : '0', 'z25' : '0', 'zhyp' : '1', 'hw' : '0', 'szt' : '1', 'faba' : '0', 'jscfcorr' : '0', 'ba08corr' : '0', 'zi' : '1', 'zr' : '0', 'arb' : '0', 'fas' : '0'
                },
                '11': {
                    'damp' : '1', 'region' : '0', 'faultType' : '1', 'magnitude' : '1', 'rrup' : '0', 'rx' : '0', 'ry0' : '0', 'rjb' : '1', 'rcd' : '0', 'df' : '0', 'ztor' : '0', 'zbot' : '0', 'width' : '0', 'delta' : '0', 'lambda' : '0', 'vs30' : '1', 'vs30type' : '0', 'z10' : '0', 'z25' : '0', 'zhyp' : '0', 'hw' : '0', 'szt' : '0', 'faba' : '0', 'jscfcorr' : '0', 'ba08corr' : '0', 'zi' : '0', 'zr' : '0', 'arb' : '0', 'fas' : '0'
                },
                '12': {
                    'damp' : '1', 'region' : '0', 'faultType' : '0', 'magnitude' : '1', 'rrup' : '0', 'rx' : '0', 'ry0' : '0', 'rjb' : '0', 'rcd' : '1', 'df' : '0', 'ztor' : '0', 'zbot' : '0', 'width' : '0', 'delta' : '0', 'lambda' : '0', 'vs30' : '1', 'vs30type' : '0', 'z10' : '0', 'z25' : '0', 'zhyp' : '1', 'hw' : '0', 'szt' : '1', 'faba' : '1', 'jscfcorr' : '0', 'ba08corr' : '0', 'zi' : '0', 'zr' : '0', 'arb' : '0', 'fas' : '0'
                },
                '13': {
                    'damp' : '1', 'region' : '0', 'faultType' : '0', 'magnitude' : '1', 'rrup' : '0', 'rx' : '0', 'ry0' : '0', 'rjb' : '0', 'rcd' : '1', 'df' : '0', 'ztor' : '0', 'zbot' : '0', 'width' : '0', 'delta' : '0', 'lambda' : '0', 'vs30' : '1', 'vs30type' : '0', 'z10' : '0', 'z25' : '0', 'zhyp' : '0', 'hw' : '0', 'szt' : '0', 'faba' : '1', 'jscfcorr' : '0', 'ba08corr' : '0', 'zi' : '0', 'zr' : '0', 'arb' : '0', 'fas' : '0'
                },
                '14': {
                    'damp' : '1', 'region' : '0', 'faultType' : '0', 'magnitude' : '1', 'rrup' : '1', 'rx' : '0', 'ry0' : '0', 'rjb' : '0', 'rcd' : '0', 'df' : '0', 'ztor' : '0', 'zbot' : '0', 'width' : '0', 'delta' : '0', 'lambda' : '0', 'vs30' : '0', 'vs30type' : '0', 'z10' : '0', 'z25' : '0', 'zhyp' : '1', 'hw' : '0', 'szt' : '1', 'faba' : '0', 'jscfcorr' : '0', 'ba08corr' : '0', 'zi' : '0', 'zr' : '1', 'arb' : '1', 'fas' : '0'
                },
                '15': {
                    'damp' : '1', 'region' : '0', 'faultType' : '0', 'magnitude' : '1', 'rrup' : '1', 'rx' : '0', 'ry0' : '0', 'rjb' : '0', 'rcd' : '0', 'df' : '0', 'ztor' : '0', 'zbot' : '0', 'width' : '0', 'delta' : '0', 'lambda' : '0', 'vs30' : '1', 'vs30type' : '0', 'z10' : '0', 'z25' : '0', 'zhyp' : '1', 'hw' : '0', 'szt' : '1', 'faba' : '0', 'jscfcorr' : '1', 'ba08corr' : '1', 'zi' : '0', 'zr' : '0', 'arb' : '0', 'fas' : '0'
                }
            };
        
            $( document ).ready(function() {
                $(params).each(function( paramIndex ) {
                    $("#" + params[paramIndex]).hide();
                    $('label[for='+params[paramIndex]+']').hide();
                });
                
                addRow();
                $("body").tooltip({ selector: '[data-toggle=tooltip]' });
            });
        
            function addRow(obj)
            {
                $('#att_table').append('<tr><td><a class="btn btn-xs btn-danger" onClick="removeRow(this);"><i class="fa fa-times"></i></a></td><td><select name="gmpes[]" class="form-control" style="width: 300px" onchange="gmpeChange();" required="required"><option value="" disabled selected>Select a GMPE</option><option value="" disabled>NGA West 2</option><option value="0">&nbsp;&nbsp;Abrahamson-Silva-Kamai 2014</option><option value="1">&nbsp;&nbsp;Boore-Stewart-Seyhan-Atkinson 2014</option><option value="2">&nbsp;&nbsp;Campbell-Bozorgnia 2014</option><option value="3">&nbsp;&nbsp;Chiou-Youngs 2014</option><option value="4">&nbsp;&nbsp;Idriss 2014</option><option value="" disabled>NGA West</option><option value="5">&nbsp;&nbsp;Abrahamson-Silva 2008</option><option value="6">&nbsp;&nbsp;Boore-Atkinson 2008</option><option value="7">&nbsp;&nbsp;Campbell-Bozorgnia 2008</option><option value="8">&nbsp;&nbsp;Chiou-Youngs 2008</option><option value="9">&nbsp;&nbsp;Idriss 2008</option><option value="" disabled>Subduction Zone</option><option value="10">&nbsp;&nbsp;Atkinson-Boore 2003</option><option value="11">&nbsp;&nbsp;Atkinson-Macias 2009</option><option value="12">&nbsp;&nbsp;BC Hydro 2013</option><option value="13">&nbsp;&nbsp;Ghofrani-Atkinson 2013</option><option value="14">&nbsp;&nbsp;Youngs 1997</option><option value="15">&nbsp;&nbsp;Zhao et al 2006</option></select></td><td><input type="text" class="form-control" placeholder="Weight" min="0" max="1"></td></tr>');
            }
            
            function removeRow(obj)
            {
                $(obj).closest ('tr').remove ();
                gmpeChange();
            }
            
            function gmpeChange()
            {
                var gmpes = $("[name='gmpes[]']");
                $(params).each(function( paramIndex ) {
                    var hasParam = false;
                    $(gmpes).each(function( gmpeIndex ) {
                        if (paramMatrix[$(gmpes[gmpeIndex]).val()][params[paramIndex]] == "1")
                        {
                            hasParam = true;
                            return false;
                        }
                    });
                    
                    if (hasParam)
                    {
                        //alert("HAS " + params[paramIndex]);
                        $("#" + params[paramIndex]).show();
                        $('label[for='+params[paramIndex]+']').show();
                    }
                    else
                    {
                        //alert("DOESNT HAVE '" + params[paramIndex] + "'");
                        $("#" + params[paramIndex]).hide();
                        $('label[for='+params[paramIndex]+']').hide();
                    }
                });
            }
        </script>
        
        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <h1 class="page-header">Ground Motion Prediction Equations (GMPEs)</h1>
                    <form class="form-inline" method="post" action="gmpe_search.php">
                        <div class="col-lg-6">
                            <table class="table" id="att_table">
                                <thead>
                                    <th><a onclick="addRow(this);" class="btn btn-xs btn-success"><i class="fa fa-plus"></i></a></th>
                                    <th>GMPE</th>
                                    <th>Weight</th>
                                </thead>
                            </table>
                        </div>
                        <div name="params" class="col-lg-6">
                            <div class="row" style="">
                                <div class="col-lg-3">
                                <label for="damp" class="required">Damping Ratio</label>
                                </div>
                                <div class="col-lg-3">
                                <select id="damp" name="damp" class="form-control" style="margin-bottom: 10px" data-toggle="tooltip"  title="DESCRIPTION" >
                                    <option value="0">5%</option>
                                </select>
                                </div>
                            </div>
                            <div class="row" style="">
                                <div class="col-lg-3">
                                    <label for="region" class="required">Region</label>
                                </div>
                                <div class="col-lg-3">
                                    <select id="region" name="region" class="form-control" style="margin-bottom: 10px" data-toggle="tooltip"  title="DESCRIPTION" >
                                        <option value="0">Global/California</option>
                                        <option value="1">China</option>
                                        <option value="2">Italy</option>
                                        <option value="3">Japan</option>
                                        <option value="4">Taiwan</option>
                                        <option value="5">Turkey</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row" style="">
                                <div class="col-lg-3">
                                    <label for="faultType" class="required">FaultType</label>
                                </div>
                                <div class="col-lg-3">
                                    <select id="faultType" name="faultType" class="form-control" style="margin-bottom: 10px" data-toggle="tooltip"  title="DESCRIPTION" >
                                        <option value="0">Unspecified</option>
                                        <option value="1">Strike Slip</option>
                                        <option value="2">Normal/Oblique</option>
                                        <option value="3">Reverse/Oblique</option>
                                    </select>
                                </div>
                            </div>

                            <div class="row" style="">
                                <div class="col-lg-3">
                                    <label for="magnitude" class="required">Magnitude (M<sub>w</sub>)</label>
                                </div>
                                <div class="col-lg-3">
                                    <input id="magnitude" name="magnitude" type="text" class="form-control" style="margin-bottom: 10px" data-toggle="tooltip"  title="DESCRIPTION" placeholder="INSIDETEXT" />
                                </div>
                            </div>
                            
                            <div class="row" style="">
                                <div class="col-lg-3">
                                    <label for="rrup" class="required">R<sub>rup</sub> (km)</label>
                                </div>
                                <div class="col-lg-3">
                                    <input id="rrup" name="rrup" type="text" class="form-control" style="margin-bottom: 10px" data-toggle="tooltip"  title="DESCRIPTION" placeholder="INSIDETEXT" />
                                </div>
                            </div>
                            
							<div class="row" style="">
								<div class="col-lg-3">
									<label for="rx" class="required">R<sub>x</sub> (km)</label>
								</div>	
								<div class="col-lg-3">
									<input id="rx" name="rx" type="text" class="form-control" style="margin-bottom: 10px" data-toggle="tooltip"  title="DESCRIPTION" placeholder="INSIDETEXT" />
								</div>
							</div>
							
							<div class="row" style="">
								<div class="col-lg-3">
									<label for="ry0" class="required">R<sub>y0</sub> (km)</label>
								</div>
								<div class="col-lg-3">
									<input id="ry0" name="ry0" type="text" class="form-control" style="margin-bottom: 10px" data-toggle="tooltip"  title="DESCRIPTION" placeholder="INSIDETEXT" />
								</div>
							</div>

							<div class="row" style="">
								<div class="col-lg-3">
									<label for="rjb" class="required">R<sub>jb</sub> (km)</label>
								</div>
								<div class="col-lg-3">
									<input id="rjb" name="rjb" type="text" class="form-control" style="margin-bottom: 10px" data-toggle="tooltip"  title="DESCRIPTION" placeholder="INSIDETEXT" />
								</div>
							</div>
							
							<div class="row" style="">
								<div class="col-lg-3">
									<label for="rcd" class="required">R<sub>cd</sub> (km)</label>
								</div>
								<div class="col-lg-3">
									<input id="rcd" name="rcd" type="text" class="form-control" style="margin-bottom: 10px" data-toggle="tooltip"  title="DESCRIPTION" placeholder="INSIDETEXT" />
								</div>
							</div>

							<div class="row" style="">
								<div class="col-lg-3">
									<label for="df" class="required">D<sub>f</sub> (km)</label>
								</div>
								<div class="col-lg-3">
									<input id="df" name="df" type="text" class="form-control" style="margin-bottom: 10px" data-toggle="tooltip"  title="DESCRIPTION" placeholder="INSIDETEXT" />
								</div>
							</div>
							
							<div class="row" style="">
								<div class="col-lg-3">
									<label for="ztor" class="required">Z<sub>tor</sub> (km)</label>
								</div>
								<div class="col-lg-3">
									<input id="ztor" name="ztor" type="text" class="form-control" style="margin-bottom: 10px" data-toggle="tooltip"  title="DESCRIPTION" placeholder="INSIDETEXT" />
								</div>
							</div>
							
							<div class="row" style="">
								<div class="col-lg-3">
									<label for="zbot" class="required">Z<sub>bot</sub> (km)</label>
								</div>
									<div class="col-lg-3">
										<input id="zbot" name="zbot" type="text" class="form-control" style="margin-bottom: 10px" data-toggle="tooltip"  title="DESCRIPTION" placeholder="INSIDETEXT" />
									</div>
							</div>
							
							<div class="row" style="">
								<div class="col-lg-3">
									<label for="width" class="required">Width (km)</label>
								</div>
								<div class="col-lg-3">
									<input id="width" name="width" type="text" class="form-control" style="margin-bottom: 10px" data-toggle="tooltip"  title="DESCRIPTION" placeholder="INSIDETEXT" />
								</div>
							</div>
							
							<div class="row" style="">
								<div class="col-lg-3">
									<label for="delta" class="required">Dip (deg)</label>
								</div>
								<div class="col-lg-3">
									<input id="delta" name="delta" type="text" class="form-control" style="margin-bottom: 10px" data-toggle="tooltip"  title="DESCRIPTION" placeholder="INSIDETEXT" />
								</div>
							</div>
							
							<div class="row" style="">
								<div class="col-lg-3">
									<label for="lambda" class="required">Rake (deg)</label>
								</div>
								<div class="col-lg-3">
									<input id="lambda" name="lambda" type="text" class="form-control" style="margin-bottom: 10px" data-toggle="tooltip"  title="DESCRIPTION" placeholder="INSIDETEXT" />
								</div>
							</div>
							
							<div class="row" style="">
								<div class="col-lg-3">
									<label for="vs30" class="required">V<sub>s30</sub> (m/s)</label>
								</div>
								<div class="col-lg-3">
									<input id="vs30" name="vs30" type="text" class="form-control" style="margin-bottom: 10px" data-toggle="tooltip"  title="DESCRIPTION" placeholder="INSIDETEXT" />
								</div>
							</div>
							
							<div class="row" style="">
								<div class="col-lg-3">
									<label for="vs30type" class="required">V<sub>s30</sub> type</label>
								</div>
								<div class="col-lg-3">
									<select id="vs30type" name="vs30type" class="form-control" style="margin-bottom: 10px" data-toggle="tooltip"  title="DESCRIPTION" >
									<option value="0">Measured</option>
									<option value="1">Estimated/Inferred</option>
									</select>
								</div>
							</div>
							
							<div class="row" style="">
								<div class="col-lg-3">
									<label for="z10" class="required">Z<sub>1.0</sub> (km)</label>
								</div>
								<div class="col-lg-3">
									<input id="z10" name="z10" type="text" class="form-control" style="margin-bottom: 10px" data-toggle="tooltip"  title="DESCRIPTION" placeholder="INSIDETEXT" />
								</div>
							</div>
							
							<div class="row" style="">
								<div class="col-lg-3">
									<label for="z25" class="required">Z<sub>2.5</sub> (km)</label>
								</div>
								<div class="col-lg-3">
									<input id="z25" name="z25" type="text" class="form-control" style="margin-bottom: 10px" data-toggle="tooltip"  title="DESCRIPTION" placeholder="INSIDETEXT" />
								</div>
							</div>
							
							<div class="row" style="">
								<div class="col-lg-3">
									<label for="zhyp" class="required">Z<sub>hyp</sub> (km)</label>
								</div>
								<div class="col-lg-3">
									<input id="zhyp" name="zhyp" type="text" class="form-control" style="margin-bottom: 10px" data-toggle="tooltip"  title="DESCRIPTION" placeholder="INSIDETEXT" />
								</div>
							</div>
							
							<div class="row" style="">
								<div class="col-lg-3">
									<label for="hw" class="required">Hanging Wall</label>
								</div>
								<div class="col-lg-3">
									<select id="hw" name="hw" class="form-control" style="margin-bottom: 10px" data-toggle="tooltip"  title="DESCRIPTION" >
									<option value="0">None</option>
									<option value="1">Hanging Wall</option>
									</select>
								</div>
							</div>
							
							<div class="row" style="">
								<div class="col-lg-3">
									<label for="szt" class="required">Subduction Zone Type</label>
								</div>
								<div class="col-lg-3">
									<select id="szt" name="szt" class="form-control" style="margin-bottom: 10px" data-toggle="tooltip"  title="DESCRIPTION" >
									<option value="0">Subcrustal</option>
									<option value="1">Subduction</option>
									</select>
								</div>
							</div>
							
							<div class="row" style="">
								<div class="col-lg-3">
									<label for="faba" class="required">Forearc/Backarc</label>
								</div>
								<div class="col-lg-3">
									<select id="faba" name="faba" class="form-control" style="margin-bottom: 10px" data-toggle="tooltip"  title="DESCRIPTION" >
									<option value="0">Forearc or unknown</option>
									<option value="1">Backarc</option>
									</select>
								</div>
							</div>
							
							<div class="row" style="">
								<div class="col-lg-3">
									<label for="jscfcorr" class="required">Japan Site Correction</label>
								</div>
								<div class="col-lg-3">
									<select id="jscfcorr" name="jscfcorr" class="form-control" style="margin-bottom: 10px" data-toggle="tooltip"  title="DESCRIPTION" >
									<option value="0">Don't Apply</option>
									<option value="1">Apply</option>
									</select>
								</div>
							</div>
							
							<div class="row" style="">
								<div class="col-lg-3">
									<label for="ba08corr" class="required">BA08 reduction</label>
								</div>
								<div class="col-lg-3">
									<select id="ba08corr" name="ba08corr" class="form-control" style="margin-bottom: 10px" data-toggle="tooltip"  title="DESCRIPTION" >
									<option value="0">Don't Apply</option>
									<option value="1">Apply</option>
									</select>
								</div>
							</div>
							
							<div class="row" style="">
								<div class="col-lg-3">
									<label for="zi" class="required">Cascadia/Japan Indicator</label>
								</div>
								<div class="col-lg-3">
									<select id="zi" name="zi" class="form-control" style="margin-bottom: 10px" data-toggle="tooltip"  title="DESCRIPTION" >
									<option value="0">General</option>
									<option value="1">Cascadia</option>
									<option value="1">Japan</option>
									</select>
								</div>
							</div>
							
							<div class="row" style="">
								<div class="col-lg-3">
									<label for="zr" class="required">Rock Indicator</label>
								</div>
								<div class="col-lg-3">
									<select id="zr" name="zr" class="form-control" style="margin-bottom: 10px" data-toggle="tooltip"  title="DESCRIPTION" >
									<option value="0">Not Rock</option>
									<option value="1">Rock</option>
									</select>
								</div>
							</div>
							
							<div class="row" style="">
								<div class="col-lg-3">
									<label for="arb" class="required">Sigma Component</label>
								</div>
								<div class="col-lg-3">
									<select id="arb" name="arb" class="form-control" style="margin-bottom: 10px" data-toggle="tooltip"  title="DESCRIPTION" >
									<option value="0">Average</option>
									<option value="1">Arbitrary</option>
									</select>
								</div>
							</div>
							
							<div class="row" style="">
								<div class="col-lg-3">
									<label for="fas" class="required">Aftershock Indicator</label>
								</div>
								<div class="col-lg-3">
									<select id="fas" name="fas" class="form-control" style="margin-bottom: 10px" data-toggle="tooltip"  title="DESCRIPTION" >
									<option value="0">Mainshock Forshock or Swarm</option>
									<option value="1">Aftershock</option>
									</select>
								</div>
							</div>
                        </div>
                        <button type="submit" class="btn btn-primary btn-lg" style="width:20%;">Submit</button>
                    </form>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
</body>

</html>
