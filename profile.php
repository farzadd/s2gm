<?php require 'include/settings.php'; ?>
<?php require 'include/confirm_login.php'; ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <?php require 'modules/metadata.php'; ?>

    <?php require 'modules/css.php'; ?>
</head>

<body>

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0;">
            <?php require 'modules/header.php'; ?>
            <!-- /.navbar-header -->

            <?php require 'modules/toplinks.php'; ?>
            <!-- /.navbar-top-links -->

            <?php require 'modules/sidebar.php'; ?>
            <!-- /.navbar-static-side -->
        </nav>
        
        <?php require 'modules/footer.php'; ?>
        
        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">        
                        <h1 class="page-header">Profile</h1>
                        <?php
                            if (isset($_POST['changepass'])
                            && isset($_POST['curPass'])
                            && isset($_POST['newPass'])
                            && isset($_POST['newPassConfirm']))
                            {
                                if ($_POST['newPass'] != $_POST['newPassConfirm'])
                                {
                                    echo '<div class="alert alert-danger alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        The new passwords entered do not match.
                                    </div>';
                                }
                                else
                                {
                                    $curpass = $mysqli->real_escape_string($_POST['curPass']);
                                    $curpass = hash_hmac ( "sha512" , $curpass, $g_salt);
                                    $result = $mysqli->query("SELECT * FROM s2gm.s2gm_users WHERE id=\"" . $_SESSION['l_userid'] . "\" AND pass=\"" . $curpass . "\"");
                                
                                    if ($row = $result->fetch_row())
                                    {
                                        $pass = $mysqli->real_escape_string($_POST['newPass']);
                                        $pass = hash_hmac ( "sha512" , $pass, $g_salt);
                                        $query = "UPDATE `s2gm`.`s2gm_users` SET `pass`='$pass' WHERE `id`='" . $_SESSION['l_userid'] . "';";
                                        
                                        if (!$result = $mysqli->query($query))
                                            echo "Unable set new password, error: ", $mysqli->error;
                                    
                                        echo '<div class="alert alert-success alert-dismissable">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                            Password change complete!
                                        </div>';
                                    }
                                    else
                                    {
                                        echo '<div class="alert alert-danger alert-dismissable">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                            Your current password is not correct. Please try again.
                                        </div>';
                                    }
                                }
                            }
                            else if (isset($_POST['newuser'])
                            && isset($_POST['newName'])
                            && isset($_POST['newPass'])
                            && $_SESSION['l_admin'] >= 5)
                            {
                                $newName = $mysqli->real_escape_string($_POST['newName']);
                                $newPass = $mysqli->real_escape_string($_POST['newPass']);
                                $newPass = hash_hmac ( "sha512" , $newPass, $g_salt);
                                $result = $mysqli->query("INSERT INTO `s2gm`.`s2gm_users` (`email`, `pass`) VALUES ('$newName', '$newPass') ON DUPLICATE KEY UPDATE `pass`='$newPass';");
                                
                                if ($result)
                                {
                                    echo '<div class="alert alert-success alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        New user "' . $newName . '" created!
                                    </div>';
                                }
                                else
                                {
                                    echo '<div class="alert alert-danger alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        Failed to create new user.
                                    </div>';
                                }
                            }
                        ?>
                        <div style="width:50%; float:left; padding-right:10px;">
                            <fieldset>
                                <legend>Profile Info</legend>
                                <label>Email/Username</label> <input type="text" class="form-control" value="<?php echo $_SESSION['l_user']; ?>" disabled />
                                <br><br>
                                <legend>Recent Searches</legend>
                                No recent searches found.
                            </fieldset>
                        </div>
                        <div style="width:50%; float:right; padding-left:10px;">
                            <fieldset>
                                <legend>Change Password</legend>
                                <form role="form" method="post" action="profile.php">
                                    <input type="password" class="form-control" name="curPass" id="curPass" placeholder="Current Password" /><br>
                                    <input type="password" class="form-control" name="newPass" placeholder="New Password" /><br>
                                    <input type="password" class="form-control" name="newPassConfirm" placeholder="New Password Confirm" /><br>
                                    <button type="submit" class="btn btn-lg btn-success btn-block" name="changepass">Change Password</button>
                                </form>
                            </fieldset>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <?php if ($_SESSION['l_admin'] >= 5): ?>
                <div class="row">
                        <div class="col-lg-12">        
                            <h1 class="page-header">Admin Panel</h1>
                            <div style="width:50%; float:left; padding-right:10px;">
                                <fieldset>
                                    <legend>Create an User</legend>
                                    <form role="form" method="post" action="profile.php">
                                        <label>Email/Username</label> <input type="text" name="newName" class="form-control" /><br>
                                        <label>Password</label> <input type="password" name="newPass" class="form-control" /><br>
                                        <button type="submit" class="btn btn-lg btn-success btn-block" name="newuser">Create User</button>
                                    </form>
                                    <br><br>
                                </fieldset>
                            </div>
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                </div>
            <?php endif; ?>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->

    <?php require 'modules/js.php'; ?>

</body>

</html>
