<?php require 'include/settings.php'; ?>
<?php require 'include/confirm_login.php'; ?>

<?php
    if (!isset($_POST['GMType']))
    {
        header("Location: searchrecords.php");
        exit();
    }
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <?php require 'modules/metadata.php'; ?>

    <?php require 'modules/css.php'; ?>
</head>

<body>

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0;">
            <?php require 'modules/header.php'; ?>
            <!-- /.navbar-header -->

            <?php require 'modules/toplinks.php'; ?>
            <!-- /.navbar-top-links -->

            <?php require 'modules/sidebar.php'; ?>
            <!-- /.navbar-static-side -->
        </nav>
        
        <?php require 'modules/footer.php'; ?>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Searching Database...</h1>
                        <?php
                            if (!isset($user_data) && isset($_COOKIE['user_data']))
                            {
                                $user_data = $_COOKIE['user_data'];
                            }

                            $error = "";
                            try
                            {
                                checkParameter($_POST['GMType'], "Ground Motion Type", 0, 3, true, true, true, $error);            // Update this if you add more ground motion types
                                checkParameter($_POST['Magnitude'], "Magnitude", 0, 10, true, true, false, $error);
                                checkParameter($_POST['Epidistance'], "Epicentral Distance", 0, null, true, true, false, $error);
                                checkParameter($_POST['Hypdistance'], "Hypocentral Distance", 0, null, true, true, false, $error);
                                checkParameter($_POST['Soiltype'], "Vs30", 0, null, true, true, false, $error);
                                checkParameter($_POST['D595'], "D5-95", 0, null, true, true, false, $error);
                                checkParameter($_POST['Pulse'], "Pulse", 0, 1, true, true, true, $error);                          // Update this if you add more pulse records
                                checkParameter($_POST['Direction'], "Spectral Ordinate", 0, 8, true, true, true, $error);
                                checkParameter($_POST['NoGM'], "Max No. Records", 0, 10, true, true, true, $error);
                                checkParameter($_POST['ScalingMeth'], "Scaling Method", 0, 3, true, true, true, $error);    // Update this if you add more periods
                                if ($_POST['ScalingMeth'] == 3) checkParameter($_POST['ScalePeriod'], "Scaling Period", 0, 10, true, true, false, $error);
                                if ($_POST['ScalingMeth'] > 1) checkParameter($_POST['ScaleFact'], "Scale Factor", 0, null, true, true, false, $error);
                                checkParameter($_POST['PeriodPoint'], "Period Points", 0, 10, true, true, false, $error);
                                checkParameter($_POST['Weights'], "Weights", 0, null, true, true, false, $error);
                                checkParameter($_POST['PerDis'], "Period Discretization", 0, 1, true, true, false, $error);
                                
                                if ($error == "")
                                {
                                    $periodPoint_l = explode (',', $_POST['PeriodPoint']);
                                    $weights_l = explode (',', $_POST['Weights']);
                                    if (count($periodPoint_l) != count($weights_l))
                                        $error = "The vector for Period Points and Weights must have the same length.";
                                }
                            }
                            catch (Exception $e) {
                                $error = $e->getMessage();
                            }

                            if ($error != "")
                            {
                                echo '<div class="alert alert-danger alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        There was an error processing your request: ' . $error . '
                                    </div>';
                            }
                            elseif (isset($user_data))
                            {
                                if ($mysqli->connect_errno) {
                                    printf("Connect failed: %s\n", $mysqli->connect_error);
                                    exit();
                                }

                                $result = $mysqli->query("SELECT * FROM s2gm.s2gm_searchs WHERE user_id=\"" . $_SESSION['l_userid'] ."\" AND progress < 100 AND error = ''");

                                if ($result->fetch_row())
                                {
                                    echo '<div class="alert alert-danger alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        You already have an active search in progress. Please wait until that one completes.
                                    </div>';
                                }
                                else
                                {
                                    $aData = json_decode($user_data);

                                    $data_x = "";
                                    $data_y = "";

                                    $first = true;
                                    while ($data = array_pop($aData)) {
                                        $data_x .= "$data[0],";
                                        $data_y .= "$data[1],";
                                    }

                                    $data_x = mysqli_real_escape_string($mysqli, rtrim($data_x, ","));
                                    $data_y = mysqli_real_escape_string($mysqli, rtrim($data_y, ","));
                                    $gmType = mysqli_real_escape_string($mysqli, $_POST['GMType']);
                                    $magnitude = mysqli_real_escape_string($mysqli, $_POST['Magnitude']);
                                    $epidistance = mysqli_real_escape_string($mysqli, $_POST['Epidistance']);
                                    $hypdistance = mysqli_real_escape_string($mysqli, $_POST['Hypdistance']);
                                    $soilType = mysqli_real_escape_string($mysqli, $_POST['Soiltype']);
                                    $D595 = mysqli_real_escape_string($mysqli, $_POST['D595']);
                                    $pulse = mysqli_real_escape_string($mysqli, $_POST['Pulse']);
                                    $direction = mysqli_real_escape_string($mysqli, $_POST['Direction']);
                                    $noGM = mysqli_real_escape_string($mysqli, $_POST['NoGM']);
                                    $scalingMeth = mysqli_real_escape_string($mysqli, $_POST['ScalingMeth']);
                                    $scalePeriod = (isset($_POST['ScalePeriod']) ? mysqli_real_escape_string($mysqli, $_POST['ScalePeriod']) : "");
                                    $scaleFact = (isset($_POST['ScaleFact']) ? mysqli_real_escape_string($mysqli, $_POST['ScaleFact']) : "");
                                    $periodPoint = mysqli_real_escape_string($mysqli, $_POST['PeriodPoint']);
                                    $weights = mysqli_real_escape_string($mysqli, $_POST['Weights']);
                                    $perDis = mysqli_real_escape_string($mysqli, $_POST['PerDis']);

                                    if ($scalingMeth == 1)
                                    {
                                        $scalePeriod = "";
                                        $scaleFact = "";
                                    } elseif ($scalingMeth == 2)
                                    {
                                        $scalePeriod = "";
                                    }

                                    $query = "INSERT INTO `s2gm`.`s2gm_searchs`
                                        (`user_id`,
                                        `progress`,
                                        `data_x`,
                                        `data_y`,
                                        `gmType`,
                                        `magnitude`,
                                        `epidistance`,
                                        `hypdistance`,
                                        `soil_type`,
                                        `D595`,
                                        `pulse`,
                                        `direction`,
                                        `noGM`,
                                        `scalingMeth`,
                                        `scalePeriod`,
                                        `scaleFact`,
                                        `periodPoint`,
                                        `weights`,
                                        `perDis`)
                                        VALUES
                                        (" . $_SESSION['l_userid'] . ",
                                        0,
                                        \"$data_x\",
                                        \"$data_y\",
                                        \"$gmType\",
                                        \"$magnitude\",
                                        \"$epidistance\",
                                        \"$hypdistance\",
                                        \"$soilType\",
                                        \"$D595\",
                                        \"$pulse\",
                                        \"$direction\",
                                        \"$noGM\",
                                        \"$scalingMeth\",
                                        \"$scalePeriod\",
                                        \"$scaleFact\",
                                        \"$periodPoint\",
                                        \"$weights\",
                                        \"$perDis\");";

                                    if (!$result = $mysqli->query($query))
                                        echo "Search Failed, error: ", $mysqli->error;

                                    $search_id = mysqli_insert_id($mysqli);

                                    $fp = fsockopen($g_serverIP, $g_serverPort, $errno, $errstr, 30);
                                    socket_set_timeout($fp, 400);
                                    if (!$fp) {
                                        echo "$errstr ($errno)<br />\n";
                                    } else {
                                        fwrite($fp, $search_id);

                                        while (!feof($fp)) {
                                            $msg = fgets($fp, 2048);
                                            if ($msg != "success")
                                                echo 'Error:' + $msg;
                                        }

                                        fclose($fp);
                                    }
                                }
                            } else {
                                echo '<div class="alert alert-danger alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        No target response spectrum has been selected.
                                    </div>';
                            }

                            function checkParameter($value, $name, $min, $max, $required, $numeric, $isint, &$error)
                            {
                                if (!isset($value) || is_null($value) || empty($value))
                                {
                                    if ($required)
                                    {
                                        $error = "You have not given " . $name . " a value! This field is required.";
                                        return false;
                                    }
                                    else
                                        return true;
                                }

                                $min_value = $value;
                                $max_value = $value;
                                if (strpos($value,',') !== false)
                                {
                                    $values = explode (',', $value);
                                    $min_value = $values[0];
                                    $max_value = $values[1];
                                }

                                if ($numeric)
                                {
                                    if (!is_numeric($min_value) || !is_numeric($max_value))
                                    {
                                        $error = $name . " must be numeric!";
                                        return false;
                                    }
                                }

                                if ($isint)
                                {
                                    if (strpos($min_value,'.') !== false || strpos($max_value,'.') !== false)
                                    {
                                        $error = $name . " must be an integer! Value given: $max_value";
                                        return false;
                                    }
                                }

                                if ($min != null)
                                {
                                    if ($min_value <= $min)
                                    {
                                        $error = $name . "'s value ($min_value) is too small. The minimum value is " . $min . ".";
                                        return false;
                                    }
                                }

                                if ($max != null)
                                {
                                    if ($max_value > $max)
                                    {
                                        $error = $name . "'s value ($max_value) is too big. The maximum value is " . $max . ".";
                                        return false;
                                    }
                                }
                                
                                return true;
                            }
                        ?>
                        <script type="text/javascript" src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
                        <script>
                            $(document).ready(
                                    function() {
                                        setInterval(function() {
                                            var value = $.get('searchprogress_value.php', { id: "<?php echo $search_id; ?>"} )
                                              .done(function( data ) {
                                                if (isNaN(data))
                                                {
                                                    $('#progBar').text(data);
                                                    $('#progBar').css('width', '100%').attr('aria-valuenow', 100);
                                                } else
                                                {
                                                    if (data == "100")
                                                    {
                                                        window.location.replace("targetspectrum.php?search_id=<?php echo $search_id; ?>");
                                                    }
                                                    else if (data == "0")
                                                    {
                                                        $('#progBar').text("Initiating search. Please wait...");
                                                        $('#progBar').css('width', '100%').attr('aria-valuenow', 100);
                                                    }
                                                    else
                                                    {
                                                        $('#progBar').text(data+'%');
                                                        $('#progBar').css('width', data+'%').attr('aria-valuenow', data);
                                                    }
                                                }
                                              });;
                                        }, 500);
                                    });
                        </script>
                        <div class="progress">
                          <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 100%;" id="progBar">
                            Initiating search. Please wait...
                          </div>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->

    <?php require 'modules/js.php'; ?>

</body>

</html>
