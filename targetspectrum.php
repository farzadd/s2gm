<?php require 'include/settings.php'; ?>
<?php require 'include/confirm_login.php'; ?>

<?php
    if (isset ($_POST['request_type']))
    {
        $request_type = $_POST['request_type'];
        
        setcookie("request_type", $request_type);
        setcookie("search_id", "", time()-3600);
        unset($search_id);
        
        if ($request_type == "bc_database")
        {
            $spectra_database = $_POST['spectra_database'];
            $region = $_POST['region'];
            $selectClass = $_POST['siteclass'];

            if (isset($spectra_database) &&
            isset($region) &&
            isset($selectClass))
            {
                setcookie("spectra_database", $spectra_database);
                setcookie("region", $region);
                setcookie("selectClass", $selectClass);
                
                if ($mysqli->connect_errno) {
                    printf("Connect failed: %s\n", $mysqli->connect_error);
                    exit();
                }
                
                $spectra_database = $mysqli->real_escape_string($spectra_database);
                $region = $mysqli->real_escape_string($region);
                $selectClass = $mysqli->real_escape_string($selectClass);
                
                $result = $mysqli->query("SELECT s.point_1,s.point_2,s.point_3,s.point_4,s.point_5,s.point_6,s.point_7,s.point_8,s.point_9 FROM s2gm.s2gm_spectra as s INNER JOIN s2gm.s2gm_regions r ON s.region = r.name WHERE database_id='1' AND year = $spectra_database AND r.id=$region;");
                
                $row = $result->fetch_row();
                $result->close();
                
                $user_data = "[[10.0,$row[8]],[5.0,$row[7]],[2.0,$row[6]],[1.0,$row[5]],[0.5,$row[4]],[0.3,$row[3]],[0.2,$row[2]],[0.1,$row[1]],[0,$row[0]]]";
                setcookie("user_data", $user_data);
            }
        } else if ($request_type == "user_defined")
        {
            $user_data = $_POST['user_data'];
            
            setcookie("user_data", $user_data);
        }
    }
    
    if (isset($_GET['search_id']))
    {
        $search_id = $_GET['search_id'];
        setcookie("search_id", $_GET['search_id']);
    } elseif (isset($_COOKIE['search_id']) && !isset($request_type))
        $search_id = $_COOKIE['search_id'];
        
    if (isset($search_id) && $search_id != 0)
    {
        if ($mysqli->connect_errno) {
            printf("Connect failed: %s\n", $mysqli->connect_error);
            exit();
        }
        
        $result = $mysqli->query("SELECT * FROM s2gm.s2gm_searchs WHERE id=$search_id;");
        if ($row = $result->fetch_row())
        {
            if ($row[18] != "")
            {
                // Should be updated when changes to search table is made <----
                $period_points = $row[18];
                $pp_exploded = explode(',', $period_points);
                $plot_min = $pp_exploded[0];
                $plot_max = $pp_exploded[count($pp_exploded)-1];
                
            }
            $result->close();
        }
        
        $result = $mysqli->query("SELECT * FROM `s2gm`.`s2gm_search_spectras` WHERE search_id=$search_id ORDER BY record_number DESC;");
        
        $spectras = array();
        while ($row = $result->fetch_row())
        {
            array_push($spectras, $row);
        }
        $result->close();
        
        $result = $mysqli->query("SELECT period, mean, mean_plus_std, mean_minus_std FROM s2gm.s2gm_search_results WHERE search_id=$search_id ORDER BY period DESC;");
        
        $result_mean = array();
        $result_mean_plus_std = array();
        $result_mean_minus_std = array();
        while ($row = $result->fetch_row())
        {
            if ($row[0] == 0)
                $row[0] = 0.01;
            if ($row[1] == 0)
                $row[1] = 0.01;
            if ($row[2] == 0)
                $row[2] = 0.01;
            if ($row[3] == 0)
                $row[3] = 0.01;
            array_push($result_mean, "[" . $row[0] . "," . $row[1] . "]");
            array_push($result_mean_plus_std, "[" . $row[0] . "," . $row[2] . "]");
            array_push($result_mean_minus_std, "[" . $row[0] . "," . $row[3] . "]");
        }
        
        $result = $mysqli->query("SELECT record_number, period, value FROM s2gm.s2gm_search_spectra_values WHERE search_id=$search_id ORDER BY record_number DESC;");
        
        while ($row = $result->fetch_row())
        {
            if ($row[0] == 0)
                $row[0] = 0.01;
            if ($row[1] == 0)
                $row[1] = 0.01;
            if ($row[2] == 0)
                $row[2] = 0.01;
                
            if (!isset($result_groundmotion[$row[0]])) $result_groundmotion[$row[0]] = array();
            array_push($result_groundmotion[$row[0]], "[" . $row[1] . "," . $row[2] . "]");
        }
    }
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <?php require 'modules/metadata.php'; ?>
    
    <script src="bower_components/jquery/dist/jquery.min.js"></script>
    <script src="js/highcharts.js"></script>
    <script src="http://code.highcharts.com/modules/data.js"></script>
    <script src="http://code.highcharts.com/modules/exporting.js"></script>

    <!-- Additional files for the Highslide popup effect -->
    <script type="text/javascript" src="http://www.highcharts.com/media/com_demo/highslide-full.min.js"></script>
    <script type="text/javascript" src="http://www.highcharts.com/media/com_demo/highslide.config.js" charset="utf-8"></script>
    <link rel="stylesheet" type="text/css" href="http://www.highcharts.com/media/com_demo/highslide.css" />
    
    <?php require 'modules/css.php'; ?>
</head>

<body>

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0;">
            <?php require 'modules/header.php'; ?>
            <!-- /.navbar-header -->

            <?php require 'modules/toplinks.php'; ?>
            <!-- /.navbar-top-links -->

            <?php require 'modules/sidebar.php'; ?>
            <!-- /.navbar-static-side -->
        </nav>

        <?php require 'modules/footer.php'; ?>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h2 class="page-header">Target Response Spectrum</h1>

                        <?php if (!isset($user_data) && !isset($_COOKIE['user_data'])): ?>
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            No target response spectrum has been selected.
                        </div>
                        <?php else: ?>
                        <div style="float:right;">
                          <select class="form-control" onchange="onTypeChange(this.value);" style="width:200px;">
                              <option value="0">Linear</option>
                              <option value="1">Semi Log X</option>
                              <option value="2">Semi Log Y</option>
                              <option value="3">Log Log</option>
                          </select>
                        </div>
                        <br><br>
                        <div id="spectra" style="min-width: 310px; height: 640px; margin: 0 auto;"></div>
                        <br><br>
                        <?php if(isset($search_id) && $search_id != 0): ?>
                        <script>
                            function selectionChanged(value)
                            {
                                if (value == true)
                                    document.getElementById("download").value = Number(document.getElementById("download").value) + 1;
                                else
                                    document.getElementById("download").value = Number(document.getElementById("download").value) - 1;

                                if (Number(document.getElementById("download").value) > 0)
                                    document.getElementById("download").disabled = false;
                                else
                                    document.getElementById("download").disabled = true;
                            }
                        </script>
                        <form method="post" action="download_motions.php">
                            <input type="hidden" name="search_id" value="<?php echo $search_id; ?>" />
                            <table class="table table-bordered table-hover">
                                <script>
                                    function selectAll() {
                                        var x = document.getElementsByTagName("input");
                                        var i;
                                        for (i = 0; i < x.length; i++) {
                                            if (x[i].type == "checkbox") {
                                                x[i].checked = true;
                                            }
                                        }

                                        document.getElementById("download").disabled = false;
                                    }
                                </script>
                                <thead>
                                    <tr>
                                        <th>Download <a href="#selectall" id="selectall" onClick="selectAll();">(Select All)</a></th>
                                        <th>Record Number</th>
                                        <th>Source</th>
                                        <th>Database Number</th>
                                        <th>Scale Factor</th>
                                        <th>Magnitude</th>
                                        <th>Epicentral Distance (km)</th>
                                        <th>Hypocentral Distance (km)</th>
                                        <th>Vs30 (m/s)</th>
                                        <th>D5-95 (sec)</th>
                                        <th>File Name H1</th>
                                        <th>File Name H2</th>
                                        <th>File Name V</th>
                                    <tr>
                                </thead>
                                <?php while ($row = array_pop($spectras)): ?>
                                <tr>
                                    <td><input type="checkbox" class="form-control" style="height:20px; box-shadow: none;" name="gm_<?php echo $row[1]; ?>" id="gm_download" value="<?php echo $row[1]; ?>" onchange="selectionChanged(this.checked);"></input></td>
                                    <td><?php echo $row[1]; ?></td>
                                    <td><?php echo $row[2]; ?></td>
                                    <td><?php echo $row[3]; ?></td>
                                    <td><?php echo number_format($row[4], 2); ?></td>
                                    <td><?php echo number_format($row[5], 1); ?></td>
                                    <td><?php echo round($row[6], 0); ?></td>
                                    <td><?php echo round($row[7], 0); ?></td>
                                    <td><?php echo round($row[8], 0); ?></td>
                                    <td><?php echo round($row[9], 0); ?></td>
                                    <td><?php echo $row[10]; ?></td>
                                    <td><?php echo $row[11]; ?></td>
                                    <?php if ($row[12] != ''): ?>
                                    <td><?php echo $row[12]; ?></td>
                                    <?php endif; ?>
                                </tr>
                                <?php endwhile; ?>
                            </table>
                            <center><button type="submit" id="download" class="btn btn-primary btn-lg" name="download" value="0" disabled>Download Selected Ground Motions</button></center>
                        </form>
                        <br><br>
                        <?php endif; ?>
                        <script>
                            $(function () {
                                $('#spectra').highcharts({
                                        chart: {
                                            zoomType: 'x'
                                        },
                                        
                                        title: {
                                            <?php
                                                if (isset($_COOKIE['request_type']) || isset($request_type))
                                                {
                                                    if (!isset($request_type))
                                                        $request_type = $_COOKIE['request_type'];
                                                
                                                    if ($request_type == "bc_database")
                                                    {
                                                        if (isset($_COOKIE['spectra_database']) && isset($_COOKIE['region']) && isset($_COOKIE['selectClass']))
                                                        {
                                                            if (!isset($spectra_database))
                                                                $spectra_database = $_COOKIE['spectra_database'];
                                                            if (!isset($region))
                                                                $region = $_COOKIE['region'];
                                                            if (!isset($selectClass))
                                                                $selectClass = $_COOKIE['selectClass'];
                                                            
                                                            $result = $mysqli->query("SELECT name FROM `s2gm`.`s2gm_regions` WHERE id=$region;");
                                                        
                                                            if ($row = $result->fetch_row())
                                                                $region = $row[0];
                                                                
                                                            $result->close();
                                                            
                                                            echo "text: 'British Columbia Database $spectra_database, $region, $selectClass'";
                                                        }
                                                        else
                                                        {
                                                            echo "text: 'British Columbia Database'";
                                                        }
                                                    }
                                                    else if ($request_type == "user_defined")
                                                    {
                                                        echo "text: 'User Defined'";
                                                    }
                                                }
                                            ?>
                                        },

                                        subtitle: {
                                            text: ''
                                        },

                                        xAxis: [{
                                            title: {
                                                text: 'Period (sec)'
                                            },
                                            tickInterval: 1,
                                            tickWidth: 0,
                                            gridLineWidth: 1,
                                            labels: {
                                                align: 'left',
                                                x: 3,
                                                y: 20
                                            },
                                            min: 0
                                            <?php if (isset($plot_min) && isset($plot_max)): ?>
                                            ,
                                            plotBands: [{
                                                id: 'plotB_main',
                                                from: <?php echo $plot_min ?>,
                                                to: <?php echo $plot_max ?>,
                                                color: 'rgba(68, 170, 213, .2)',
                                                borderWidth: 1,
                                                borderColor: '#000000',
                                                label: {
                                                    text: 'Period Points',
                                                    textAlign: 'center'
                                                }
                                            }]<?php endif; ?>
                                        }],

                                        yAxis: [{ // left y axis
                                            title: {
                                                text: 'Spectral Accelartion (g)',
                                                x:-15
                                            },
                                            labels: {
                                                align: 'left',
                                                x: -20,
                                                y: 16,
                                                format: '{value:.,0f}'
                                            },
                                            min: 0,
                                            showFirstLabel: false
                                        }],

                                        legend: {
                                            layout: 'vertical',
                                            align: 'right',
                                            verticalAlign: 'top',
                                            y: 50,
                                            padding: 3,
                                            itemMarginTop: 5,
                                            itemMarginBottom: 5,
                                            itemStyle: {
                                                lineHeight: '14px'
                                            }
                                        },

                                        tooltip: {
                                            shared: true,
                                            crosshairs: true,
                                            valueDecimals: 3,
                                            formatter: function () {
                                                return 'Period = <b>' + this.x.toFixed(2) + ' (sec)</b><br>Sa = <b>' + this.y.toFixed(3) + ' (g)</b>';
                                            }
                                        },

                                        plotOptions: {
                                            series: {
                                                cursor: 'pointer',
                                                point: {
                                                    events: {
                                                        click: function (e) {
                                                            hs.htmlExpand(null, {
                                                                pageOrigin: {
                                                                    x: e.pageX || e.clientX,
                                                                    y: e.pageY || e.clientY
                                                                },
                                                                headingText: this.series.name,
                                                                maincontentText: 'T=' + this.x + '<br>Sa(g)=' + this.y.toFixed(3),
                                                                width: 250
                                                            });
                                                        }
                                                    }
                                                },
                                                marker: {
                                                    lineWidth: 1
                                                }
                                            }
                                        },

                                        series: [{
                                            name: 'Target Response Spectrum',
                                            data: [
                                            <?php
                                                if (!isset($user_data) && isset($_COOKIE['user_data']))
                                                {
                                                    $user_data = $_COOKIE['user_data'];
                                                }

                                                if (isset($user_data))
                                                {
                                                    $aData = json_decode($user_data);

                                                    $first = true;
                                                    while ($data = array_pop($aData)) {
                                                        if ($data[0] == 0)
                                                            $data[0] = 0.01;

                                                        if ($data[1] == 0)
                                                            $data[1] = 0.01;

                                                        if ($first)
                                                            $first = false;
                                                        else
                                                            echo ',';

                                                        echo "[$data[0],$data[1]]";
                                                    }
                                                }
                                            ?>],
                                            lineWidth: 4,
                                            color: "#2B5796",
                                            marker: {
                                                enabled: true,
                                                radius: 4
                                            },
                                            zIndex: 100
                                        }
                                        <?php if (isset($search_id) && $search_id != 0): ?>
                                        ,{
                                            name: 'Mean',
                                            data: [
                                            <?php
                                                if (isset($result_mean))
                                                {
                                                    $first = true;
                                                    while ($data = array_pop($result_mean)) {
                                                        if ($first)
                                                            $first = false;
                                                        else
                                                            echo ',';

                                                        echo "$data";
                                                    }
                                                }
                                            ?>],
                                            color: '#f5070b',
                                            lineWidth: 4,
                                            marker: {
                                                enabled: false
                                            },
                                            zIndex: 99
                                        },
                                        {
                                            name: 'Mean + stdv.',
                                            data: [
                                            <?php
                                                if (isset($result_mean_plus_std))
                                                {
                                                    $first = true;
                                                    while ($data = array_pop($result_mean_plus_std)) {
                                                        if ($first)
                                                            $first = false;
                                                        else
                                                            echo ',';

                                                        echo "$data";
                                                    }
                                                }
                                            ?>],
                                            color: '#f5070b',
                                            dashStyle: 'longdash',
                                            lineWidth: 3,
                                            marker: {
                                                enabled: false,
                                                symbol: "square"
                                            },
                                            zIndex: 98
                                        },
                                        {
                                            name: 'Mean - stdv.',
                                            data: [
                                            <?php
                                                if (isset($result_mean_minus_std))
                                                {
                                                    $first = true;
                                                    while ($data = array_pop($result_mean_minus_std)) {
                                                        if ($first)
                                                            $first = false;
                                                        else
                                                            echo ',';

                                                        echo "$data";
                                                    }
                                                }
                                            ?>],
                                            color: '#f5070b',
                                            dashStyle: 'longdash',
                                            lineWidth: 3,
                                            marker: {
                                                enabled: false,
                                                symbol: "square"
                                            },
                                            zIndex: 98
                                        },
                                        {
                                            id: 'gm',
                                            name: 'ith Ground Motions',
                                            color: '#000000',
                                            dashStyle: 'longdash',
                                            lineWidth: 1,
                                            marker: {
                                                enabled: false,
                                                symbol: "triangle"
                                            }
                                        }
                                        <?php if (isset($result_groundmotion)): ?>
                                            <?php for ($i = 1; $i <= count($result_groundmotion); $i++): ?>
                                            ,{
                                                linkedTo: 'gm',
                                                name: '<?php echo $i; ?>th Ground Motions',
                                                color: '#000000',
                                                dashStyle: 'longdash',
                                                lineWidth: 1,
                                                marker: {
                                                    enabled: false,
                                                    symbol: "triangle"
                                                },
                                                data: [
                                                <?php
                                                    if (isset($result_groundmotion[$i]))
                                                    {
                                                        $first = true;
                                                        while ($data = array_pop($result_groundmotion[$i])) {
                                                            if ($first)
                                                                $first = false;
                                                            else
                                                                echo ',';

                                                            echo "$data";
                                                        }
                                                    }
                                                ?>]
                                            }
                                            <?php endfor; ?>
                                        <?php endif; ?>
                                        <?php endif; ?>]
                                    });

                            });

                            function onTypeChange(value)
                            {
                                var chart = $('#spectra').highcharts();

                                if (value == 0)
                                {
                                    chart.yAxis[0].update({
                                        type: 'linear',
                                        min: 0
                                    });

                                    chart.xAxis[0].update({
                                        type: 'linear',
                                        min: 0,
                                        tickInterval: 1,
                                        minorTickInterval: 0
                                    });
                                }
                                else if (value == 1)
                                {
                                    chart.yAxis[0].update({
                                        type: 'linear',
                                        min: 0
                                    });

                                    chart.xAxis[0].update({
                                        type: 'logarithmic',
                                        min: 0.1
                                    });
                                }
                                else if (value == 2)
                                {
                                    chart.yAxis[0].update({
                                        type: 'logarithmic',
                                        min: 0.1
                                    });

                                    chart.xAxis[0].update({
                                        type: 'linear',
                                        min: 0,
                                        tickInterval: 1,
                                        minorTickInterval: 0
                                    });
                                }
                                else if (value == 3)
                                {
                                    chart.yAxis[0].update({
                                        type: 'logarithmic',
                                        min: 0.1
                                    });

                                    chart.xAxis[0].update({
                                        type: 'logarithmic',
                                        min: 0.1,
                                        tickInterval: 1,
                                        minorTickInterval: 0.1
                                    });
                                }
                            }
                        </script>
                        <?php endif; ?>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->

    <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>
    <script src="dist/js/sb-admin-2.js"></script>

    <?php
        if (isset($mysqli))
        {
            if ($mysqli->ping())
                $mysqli->close();
        }
    ?>
    
</body>

</html>
