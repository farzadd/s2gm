<?php
    ini_set("max_execution_time", 600);
    ini_set("include_path", '/home/shlclan/php:' . ini_get("include_path") );
    require 'include/settings.php';

    if (!isset($_POST['search_id']))
    {
        exit("Failed. Search ID not found!");
    }
    
    $search_id = $_POST['search_id'];
    
	$mysqli = new mysqli($g_db_host, $g_db_user, $g_db_pass, $g_db_name, $g_db_port);

    if ($mysqli->connect_errno) {
        printf("Connect failed: %s\n", $mysqli->connect_error);
        exit();
    }
    
    $gm_selected = array_intersect_key($_POST, array_flip(preg_grep('/^gm_/', array_keys($_POST))));
    
    if (count($gm_selected) == 0)
        exit("Failed! No ground motions selected!");
    
    $zip = new ZipArchive();
    $filename = "./downloads/s2gm_" . time() . ".zip";
    
    if ($zip->open($filename, ZipArchive::CREATE)!==TRUE) {
        exit("cannot open <$filename>\n");
    }
    
    $table = "Record Number, Source, Database Number, Scale Factor, Magnitude, Epicentral Distance (km), Hypocentral (km), Vs30 (m/s), D5-95 (sec), File Name H1, File Name H2, File Name V\r\n";
    foreach ($gm_selected as $gm)
    {
        $gm_value = $_POST["gm_".$gm];
        $result = $mysqli->query("SELECT record_number, source, nga_record, scale_factor, magnitude, distance_epi, distance_hyp, soil_Vs30, duration_595, file_name1, file_name2, file_name3 FROM s2gm.s2gm_search_spectras WHERE search_id=$search_id AND record_number=$gm_value;");
        
        if ($row = $result->fetch_row())
        {
            $table .= $row[0] . "," . $row[1] . "," . $row[2] . "," . $row[3] . "," . $row[4] . "," . $row[5] . "," . $row[6] . "," . $row[7] . "," . $row[8] . "," .  $row[9] . "," . $row[10] . "," . $row[11] . "\r\n";

            $zip->addFile("../groundmotions/" . $row[9] . ".dat", $row[9] . ".txt");
            $zip->addFile("../groundmotions/" . $row[10] . ".dat", $row[10] . ".txt");
            $zip->addFile("../groundmotions/" . $row[11] . ".dat", $row[11] . ".txt");
        }
    }

    $zip->addFromString("table_overview.txt", $table);
        
    $result->close();
    $mysqli->close();
    $zip->close();
    
    header('Content-Type: application/zip');
    header('Content-disposition: attachment; filename=s2gm_groundmotions_' . $search_id . '.zip');
    header('Content-Length: ' . filesize($filename));
    readfile($filename);
?>