<?php
    ini_set("max_execution_time", 600);
    ini_set("include_path", '/home/shlclan/php:' . ini_get("include_path") );
    require 'include/settings.php';

    if (!isset($_POST['search_id']))
    {
        exit("Failed. Search ID not found!");
    }
    
    $search_id = $_POST['search_id'];
    
	$mysqli = new mysqli($g_db_host, $g_db_user, $g_db_pass, $g_db_name, $g_db_port);

    if ($mysqli->connect_errno) {
        printf("Connect failed: %s\n", $mysqli->connect_error);
        exit();
    }
    
    $gm_selected = array_intersect_key($_POST, array_flip(preg_grep('/^gm_/', array_keys($_POST))));
    
    if (count($gm_selected) == 0)
        exit("Failed! No ground motions selected!");
    
    $zip = new ZipArchive();
    $filename = "./downloads/s2gm_" . time() . ".zip";
    
    if ($zip->open($filename, ZipArchive::CREATE)!==TRUE) {
        exit("cannot open <$filename>\n");
    }
    
    $zip->addFromString("test.txt", "test");
    foreach ($gm_selected as $gm)
    {
        $gm_value = $_POST["gm_".$gm];
        $result = $mysqli->query("SELECT file_name1, file_name2, file_name3 FROM s2gm.s2gm_search_spectras WHERE search_id=$search_id AND record_number=$gm_value;");
        
        if ($row = $result->fetch_row())
        {
            for ($i = 0; $i < 3; $i++)
            {
                $result2 = $mysqli->query("SELECT id, fullname, unit, timestep, time_interval, PA FROM s2gm.s2gm_gmaccel WHERE name=\"" . $row[$i] . ".dat\";");
                if ($gmaccel = $result2->fetch_row())
                {
                    $file = $gmaccel[1] . "\r\nunit " . $gmaccel[2] . "\r\ntimesteps " . $gmaccel[3] . "\r\ntime interval " . $gmaccel[4] . "\r\nPA = " . $gmaccel[5] . " " . $gmaccel[2];
                    
                    $result3 = $mysqli->query("SELECT value FROM s2gm.s2gm_gmaccel_values WHERE gmaccel_id=" . $gmaccel[0] . ";");
                    
                    while ($value = $result3->fetch_row())
                    {
                        $file .= "\r\n" . $value[0];
                    }
                    
                    $zip->addFromString($row[$i] . ".txt", $file);
                }
            }
        }
    }
        
    $result->close();
    $mysqli->close();
    $zip->close();
    
    header('Content-Type: application/zip');
    header('Content-disposition: attachment; filename='.$filename);
    header('Content-Length: ' . filesize($filename));
    readfile($filename);
?>