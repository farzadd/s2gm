<?php require 'include/settings.php'; ?>
<?php require 'include/confirm_login.php'; ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <?php require 'modules/metadata.php'; ?>

    <?php require 'modules/css.php'; ?>
</head>

<body>

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0;">
            <?php require 'modules/header.php'; ?>
            <!-- /.navbar-header -->

            <?php require 'modules/toplinks.php'; ?>
            <!-- /.navbar-top-links -->

            <?php require 'modules/sidebar.php'; ?>
            <!-- /.navbar-static-side -->
        </nav>
        
        <?php require 'modules/footer.php'; ?>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h2 class="page-header">Target Response Spectrum: British Columbia Database</h2>
						<div class="form-group">
						<br>
							<br>
                            <form method="POST" action="targetspectrum.php">
                                <input type="hidden" name="request_type" value="bc_database" />
                                <table class="table" style="width:50%; float:left;">
                                    <tr>
                                        <td style="border-top: 0;"> 
                                            <label for="spectra_database">Select Database: </label>
                                        </td>
                                        <td style="border-top: 0;">
                                            <select name="spectra_database" class="form-control" style="width:250px" value="" >
                                                <option value="2010">BC 2010</option>
                                                <option value="2015" <?php if (isset($_COOKIE['spectra_database']) && $_COOKIE['spectra_database'] == "2015") echo 'selected'; ?>>BC 2015</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label for="region">Select Region: </label>
                                        </td>
                                        <td>
                                            <select name="region" class="form-control" style="width:250px">
                                                <?php
                                                    if ($mysqli->connect_errno) {
                                                        printf("Connect failed: %s\n", $mysqli->connect_error);
                                                        exit();
                                                    }
                                                    
                                                    $result = $mysqli->query("SELECT * FROM s2gm.s2gm_regions;");
                                
                                                    if (isset($_COOKIE['region']))
                                                        $region = $_COOKIE['region'];
                                                    else
                                                        $region = 0;
                                                        
                                                    while ($row = $result->fetch_row())
                                                    {
                                                        echo '<option value="' . $row[0] . '" ' . (($region == $row[0]) ? 'selected' : '')  . '>' . $row[1] . '</option>';
                                                    }
                                                    
                                                    $result->close();
                                                ?>
                                            </select>
                                        </td>
                                        
                                    </tr>
                                    <tr>
                                        <td>
                                            <label for="siteclass">Select Site Class: </label>
                                        </td>
                                        <td>
                                            <select name="siteclass" class="form-control" style="width:250px">
                                                <option>Site Class C</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="border-top: 0;"></td>
                                        <td style="border-top: 0;"><br>
                                            <button type="submit" class="btn btn-primary btn-lg" style="width:50%;">Submit</button>
                                        </td>
                                    </tr>
                                </table>
                            </form>
                            <img src="media/BC.png" align="right" style="width:40%; display:inline-block; margin-right:10%"/>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->

    <?php require 'modules/js.php'; ?>

</body>

</html>
